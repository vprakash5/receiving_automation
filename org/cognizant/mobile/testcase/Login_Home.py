'''
Created on Apr 30, 2015

@author: 209537
'''
import unittest

from org.cognizant.mobile.execution.TestReporting import ResultStore, Status, TestReport
from org.cognizant.mobile.scripthelper.ObjectRepository import Element
from org.cognizant.mobile.testcase.BaseState import BaseState
from org.cognizant.mobile.testcase.Defect import Defect
from time import sleep
from datetime import datetime
#from org.cognizant.mobile.scripthelper.ReusableLibrary import ReusableLibrary
from org.cognizant.mobile.scripthelper.Utility import Utility




class Login_Home(unittest.TestCase):


    def setUp(self):
        
        print('In setup')
#         sleep(3)
        self.lib = Utility()
        
        print(self.id())
        self.report = ResultStore(self.id(), self.mobileBrowser)
        self.StartTime = datetime.now()
        
        # Invoking BaseState
        baseState = BaseState()
#         if (self.testData['BaseState'] == 'PublicUS'):
#             baseState.PublicUS(self.mobileBrowser)
#             pass
#         elif (self.testData['BaseState'] == 'PublicUK'):
#             baseState.PublicUK(self.mobileBrowser)
#             pass        
        pass

    def tearDown(self):
        self.mobileBrowser.quit()
        self.EndTime = datetime.now()
        delta = self.EndTime - self.StartTime
        self.report.setExecutionTime(delta)
        TestReport.addToReportList(self.report)                
        pass
    
    #===========================================================================
    # LOGIN SCREEN
    #===========================================================================
    
    def Copart_ReceivingApp_003(self):
        missing = ''
        try:
            verify = ['name:Log In', 'Login_Username', 'Login_Password', 'Login_SignIn_Btn']
            missing=Defect.dictionary_checkifPresent_helper(self, verify,'login',True )
            Defect.click_leftdrawer(self)
            if(Element('LeftMenu_Settings', self.mobileBrowser).isElementPresent()):
                pass
            else:
                missing=missing+',settings'
                pass
            if(missing==''):
                self.report.addReportLog("Found all elements on  Login Screen", "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog("Missing on Login Screen:"+missing, "Failed to be executed", Status.Fail)
                pass
        except AssertionError as ase:
            self.report.addReportLog('Finding elements on Login Screen', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Finding elements on Login Screen', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
            
    def Copart_ReceivingApp_004(self):
        try:
            Element('Login_SignIn_Btn', self.mobileBrowser).waitForElement(15)
#             Element('Login_SignIn_Btn', self.mobileBrowser).bringToVisibility()
            Element('Login_SignIn_Btn', self.mobileBrowser).click()
            Element('USERNAME_PASSWORD_REQUIRED', self.mobileBrowser).bringToVisibility()
            if(Element('USERNAME_PASSWORD_REQUIRED', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog("Login with blank credentials with expected error", "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('Login with blank credentials with expected error case Failed', 'Failed', Status.Fail)
                pass
        except AssertionError as ase:
            self.report.addReportLog('Login with blank credentials with expected error Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Login with blank credentials with expected error Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
            
    def Copart_ReceivingApp_005(self):
        try:
            username=self.lib.fetchDataValue(self.testData, 'UserName')
            password=self.lib.fetchDataValue(self.testData, 'Password')
            Defect.login_helper(self,username,password)
            Element('INCORRECT_USERNAME_OR_PASSWORD', self.mobileBrowser).waitForElement(3)
            if(Element('INCORRECT_USERNAME_OR_PASSWORD', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog("Login with wrong credentials with expected error", "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('Login with wrong credentials with expected error Failed', 'Fail', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('Login with wrong credentials with expected error Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Login with wrong credentials with expected error Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def Copart_ReceivingApp_006(self):
        try:
            Defect.login(self)
            Element('Receiving_List', self.mobileBrowser).waitForElement(3)
            if(Element('Receiving_List', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog("Login with Valid credentials", "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('Login with Valid credentials Failed', 'Failed to Execute', Status.Fail)
                pass
                
        except AssertionError as ase:
            self.report.addReportLog('Login with Valid credentials Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Login with Valid credentials Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    def Copart_ReceivingApp_007(self):
        try:
            #DISABLE THE WIFI HERE
            Defect.login(self)
            Element('name:Home', self.mobileBrowser).bringToVisibility()
            if(Element('name:Home', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog("Entered login credentials", "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('Entering Login credentials Failed', 'Fail', Status.Fail)
                pass
        except AssertionError as ase:
            self.report.addReportLog('Entering Login credentials Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Entering Login credentials Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    pass
    
    def Copart_ReceivingApp_008(self):
        try:
            Defect.login(self)
            Defect.sign_out(self)
            Element('Login_Username', self.mobileBrowser).bringToVisibility()
            if(Element('Login_Username', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog("Sign out", "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('Sign out Failed', 'Fail', Status.Fail)
                pass
                
        except AssertionError as ase:
            self.report.addReportLog('Sign out Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Sign Out Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def Copart_ReceivingApp_010(self):
        try:
            Element('Login_Username', self.mobileBrowser).waitForElement(3)
            Element('Login_Username', self.mobileBrowser).setText('ABCD')
            Element('Login_Password', self.mobileBrowser).waitForElement(3)
            Element('Login_Password', self.mobileBrowser).setText('EFGH')
            Element('Login_SignIn_Btn', self.mobileBrowser).bringToVisibility()
            Element('Login_SignIn_Btn', self.mobileBrowser).click()    
            Element('name:OK', self.mobileBrowser).waitForElement(3)
            Element('name:OK', self.mobileBrowser).click()
            Element('Login_SignIn_Btn', self.mobileBrowser).waitForElement(3)
            Element('Login_SignIn_Btn', self.mobileBrowser).click()
            Element('INCORRECT_PASSWORD_ATTEMPTS',self.mobileBrowser).waitForElement(3)
            if(Element('INCORRECT_PASSWORD_ATTEMPTS',self.mobileBrowser).isElementPresent()):
                self.report.addReportLog("Entered incorrect password twice and saw appropriate error message", "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog("Entering incorrect password twice - error message did not show up", "Failed", Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('Entering incorrect password twice behavior Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Entering incorrect password twice  behavior Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    #===========================================================================
    # HOME SCREEN
    #===========================================================================
    def Copart_ReceivingApp_011(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Element('Search_TextField', self.mobileBrowser).waitForElement(20)
            Element('Search_TextField', self.mobileBrowser).bringToVisibility()
            if(Element('Search_TextField', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog("Login and tap Receiving icon", "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('Login and tap Receiving icon', 'Fail', Status.Fail)
                pass
        except AssertionError as ase:
            self.report.addReportLog('Login and tap Receiving icon', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Login and tap Receiving icon', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def Copart_ReceivingApp_012(self):
        try:
            Defect.login(self)
            Element('name:Home', self.mobileBrowser).waitForElement(15)
            if(Element('name:Home', self.mobileBrowser).isElementPresent()):
                Element('Home_Yard', self.mobileBrowser).waitForElement(15)
                Element('Home_Yard', self.mobileBrowser).click()
                temp = self.lib.fetchDataValue(self.testData,'YardSearchText')
                Element('id:'+temp, self.mobileBrowser).bringToVisibility()
                Element('id:'+temp, self.mobileBrowser).click()
                check = ['Receiving_List', 'Images_Decoup']
                missing = Defect.dictionary_checkifPresent_helper(self, check, 'Home', True)
                if(missing==''):
                    Defect.click_leftdrawer(self)
                    check2 = ['LeftMenu_HomeScreen','LeftMenu_Settings','Sign_Out']
                    missing2 = Defect.dictionary_checkifPresent_helper(self, check2, 'Left Menu', True)
                    if(missing2==''):
                        self.report.addReportLog("Checking Home element after login", "Successfully executed", Status.Pass)
                        pass
                    else:
                        self.report.addReportLog("Checking Home element after login. Missing:"+missing2, "Failed", Status.Fail)

                else:
                    self.report.addReportLog("Checking Home element after login. Missing:"+missing, "Failed", Status.Fail)
                    pass
                pass
            else:
                self.report.addReportLog("Checking Home element after login", "Failed", Status.Fail)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Missing Home/Receiving/Images icon or Left Drawer contents', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Missing Home/Receiving/Images icon or Left Drawer contents', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def Copart_ReceivingApp_013(self):
        try:
            Defect.login(self)
            Defect.get_imagedecouplinglist(self)
            lotnum = 'Lot_Images_0'
            Element(lotnum, self.mobileBrowser).waitForElement(15)
            Element(lotnum,self.mobileBrowser).bringToVisibility()
            if(Element(lotnum, self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Tapped on Images icon and found lots','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Tapping on Images icon to find lots','Failed', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_016(self):
        try:
            missing=''
            Login_Home.Copart_ReceivingApp_013(self)
            Element('Sort_Button', self.mobileBrowser).bringToVisibility()
            Element('Sort_Button', self.mobileBrowser).click()
            dictionary = ['Sort_Lot_High2Low','Sort_Lot_Low2High','Sort_Model_A2Z', 'Sort_Model_Z2A','Sort_SellerType_A2Z','Sort_SellerType_Z2A', 'Sort_TripTime_L2O','Sort_TripTime_O2L','Sort_RowLoc_A2Z','Sort_RowLoc_Z2A' ]
            missing=Defect.dictionary_checkifPresent_helper(self, dictionary, 'Images Sort', True)
            if(missing==''):
                self.report.addReportLog('Tapped on Sort and found options','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Tapped on Sort and found no options','Successfully Executed', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        pass
    def Copart_ReceivingApp_017(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            tempsort1 = self.lib.fetchDataValue(self.testData, 'Sort1')
            tempsort2 =self.lib.fetchDataValue(self.testData, 'Sort2')
            Element('Sort_Button', self.mobileBrowser).waitForElement(20)
            Element('Sort_Button', self.mobileBrowser).bringToVisibility()
            Element('Sort_Button', self.mobileBrowser).click()
            Element(tempsort1, self.mobileBrowser).bringToVisibility()
            Element(tempsort1, self.mobileBrowser).click()
            sleep(2)
            Element('Sort_Button', self.mobileBrowser).waitForElement(20)
            Element('Sort_Button', self.mobileBrowser).bringToVisibility()
            Element('Sort_Button', self.mobileBrowser).click()
            Element(tempsort2, self.mobileBrowser).bringToVisibility()
            Element(tempsort2, self.mobileBrowser).click()
            Element('Search_TextField',self.mobileBrowser).bringToVisibility()
            if(Element('Search_TextField',self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Tapped on Sort and selected an option.Tapped sort and selected diff option','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Tapped on Sort and selected an option','Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_018(self):
        try:
            Defect.login(self)
            Defect.get_imagedecouplinglist(self)
            Element('Sort_Button', self.mobileBrowser).waitForElement(20)
            Element('Sort_Button', self.mobileBrowser).bringToVisibility()
            Element('Sort_Button', self.mobileBrowser).click()
            Element('BackArrow', self.mobileBrowser).bringToVisibility()
            Element('BackArrow', self.mobileBrowser).click()
            Element('Search_TextField',self.mobileBrowser).bringToVisibility()
            if(Element('Search_TextField',self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Tapped on Sort and clicked back arrow.Returned to list','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Tapping on sort, clicking back arrow','Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_019(self):
        try:
            dictionary = ['name:Filter by','BackArrow', 'name:Apply', 'name:Model','name:Seller', 'name:Seller Type','name:Trip Confirmation', 'name:Row Location']
            Defect.login(self)
            Defect.get_imagedecouplinglist(self)
            Element('Filter_Button', self.mobileBrowser).waitForElement(15)
            Element('Filter_Button', self.mobileBrowser).bringToVisibility()
            Element('Filter_Button', self.mobileBrowser).click()
            missing=Defect.dictionary_checkifPresent_helper(self, dictionary, 'Images Filter Screen', True)
            if(missing==''):
                self.report.addReportLog('Tapped on Filter on Images tab and found all elements','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Tapping on Filter for Images tab,missing '+missing,'Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    def Copart_ReceivingApp_020(self):
        try:
            tempFilterType = self.lib.fetchDataValue(self.testData, 'FilterByType')
            tempFilterValue = self.lib.fetchDataValue(self.testData, 'FilterByValue')
            Defect.login(self)
            Defect.get_receivinglist(self)
            Element('Filter_Button', self.mobileBrowser).waitForElement(15)
            Element('Filter_Button', self.mobileBrowser).bringToVisibility()
            Element('Filter_Button', self.mobileBrowser).click()
            dictionary= ['id:Model_Any', 'id:Seller_Any', 'id:SellerType_Any', 'id:TripTime_Any']#PROBLEM HERE THESE IDS DONT EXIST
            missing=Defect.dictionary_checkifPresent_helper(self, dictionary, 'Filter Receiving list', True)
            if(missing!=''):
                Element('name:'+tempFilterType, self.mobileBrowser).bringToVisibility()
                Element('name:'+tempFilterType, self.mobileBrowser).click()
                Element('name:'+tempFilterValue, self.mobileBrowser).bringToVisibility()
                Element('name:'+tempFilterValue, self.mobileBrowser).click()
                Element('name:Apply', self.mobileBrowser).bringToVisibility()
                Element('name:Apply', self.mobileBrowser).click()
                self.report.addReportLog('Tapped on Filter on Receiving tab, Default value is Any. Selected new value for filter','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Tapping on Filter for Receiving tab,missing '+missing,'Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_021(self):
        try:
            tempFilterType = self.lib.fetchDataValue(self.testData, 'FilterByType')
            tempFilterValue = self.lib.fetchDataValue(self.testData, 'FilterByValue')
            Defect.login(self)
            Defect.get_receivinglist(self)
            Element('Filter_Button', self.mobileBrowser).waitForElement(15)
            Element('Filter_Button', self.mobileBrowser).bringToVisibility()
            Element('Filter_Button', self.mobileBrowser).click()
            Element('name:'+tempFilterType, self.mobileBrowser).bringToVisibility()
            Element('name:'+tempFilterType, self.mobileBrowser).click()
            Element('BackArrow', self.mobileBrowser).bringToVisibility()
            Element('BackArrow', self.mobileBrowser).click()
            Element('name:'+tempFilterType, self.mobileBrowser).bringToVisibility()
            Element('name:'+tempFilterType, self.mobileBrowser).click()
            Element('name:'+tempFilterValue, self.mobileBrowser).bringToVisibility()
            Element('name:'+tempFilterValue, self.mobileBrowser).click()
            Element('name:Apply', self.mobileBrowser).bringToVisibility()
            Element('name:Apply', self.mobileBrowser).click()
            Element('Search_TextField', self.mobileBrowser).waitForElement(10)
            if(Element('Search_TextField', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Tapped on Filter on Receiving tab, Tap Filter, FilterType. Then Tap Arrow, then Selected new value for filter','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Trying to tap on Filter on Receiving tab, Tap Filter, FilterType. Then Tap Arrow, then Selected new value for filter','Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_022(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Element('Filter_Button', self.mobileBrowser).waitForElement(15)
            Element('Filter_Button', self.mobileBrowser).click()
            for i in (1,4):
                tempFilterType = self.lib.fetchDataValue(self.testData, 'FilterByType'+str(i))
                tempFilterValue = self.lib.fetchDataValue(self.testData, 'FilterByValue'+str(i))
                Element('name:'+tempFilterType,self.mobileBrowser).bringToVisibility()
                Element('name:'+tempFilterType,self.mobileBrowser).click()
                Element('name:'+tempFilterValue,self.mobileBrowser).bringToVisibility()
                Element('name:'+tempFilterValue,self.mobileBrowser).click()
                pass
            Element('name:Apply', self.mobileBrowser).bringToVisibility()
            Element('name:Apply', self.mobileBrowser).click()
            temp = self.lib.fetchDataValue(self.testData, 'FilterByValue1')
            if(temp=='Any'):
                temp='Search'
                pass
            Element('name:'+temp, self.mobileBrowser).waitForElement(5)
            if(Element('name:'+temp, self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Receiving List-Selected new value for filter values by Model, Seller, Seller Type and Trip Confirmation','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Trying to tap on Filter on Receiving tab and Select new value for filter options','Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_024(self):
        try:
            Defect.login(self)
            Element('Home_Yard', self.mobileBrowser).bringToVisibility()
            Element('Home_Yard', self.mobileBrowser).click()
            temp = self.lib.fetchDataValue(self.testData,'YardSearchText')
            temp2 = self.lib.fetchDataValue(self.testData,'YardSearch_Num')
            Element('id:'+temp, self.mobileBrowser).waitForElement(3)
            Element('id:'+temp, self.mobileBrowser).click()
            Element('Receiving_List',self.mobileBrowser).waitForElement(4)
            Element('Receiving_List',self.mobileBrowser).click()
            Element('Lot_0',self.mobileBrowser).waitForElement(14)
            if(Element('Lot_0',self.mobileBrowser).isElementPresent()):
                Defect.click_leftdrawer(self)
                Element('LeftMenu_HomeScreen', self.mobileBrowser).click()
                Element('Home_Yard', self.mobileBrowser).bringToVisibility()
                Element('Home_Yard', self.mobileBrowser).click()
                Element('Yard_Search', self.mobileBrowser).waitForElement(3)
                Element('Yard_Search', self.mobileBrowser).setText(temp2)
                Element.presskey(self, keycode=66)
                temp3 = 'id:yard_'+temp2+'.'
                if(Element(temp3, self.mobileBrowser).isElementPresent()):
                    Element('BackArrow', self.mobileBrowser).click()
                    if(Element('Home_Yard', self.mobileBrowser).isElementPresent()):
                        self.report.addReportLog('Verified Yard elements and searched yard','Successfully Executed', Status.Pass)
                        pass
                    else:
                        self.report.addReportLog('Failed to verify yard elements','Failed to Execute', Status.Fail)
                        pass
                    pass
                else:
                    self.report.addReportLog('Failed to verify yard elements','Failed to Execute', Status.Fail)
                    pass
                pass
            else:
                self.report.addReportLog('Failed to verify yard elements','Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_025(self):
        try:
            missing=''
            success=''
            Defect.login(self)
            Defect.click_leftdrawer(self)
            dictionary = ['LeftMenu_HomeScreen','LeftMenu_Settings','Sign_Out']
            missing = Defect.dictionary_checkifPresent_helper(self, dictionary, 'Left Menu', True)
            if(missing==''):#i5
                Element('LeftMenu_Settings', self.mobileBrowser).bringToVisibility()
                Element('LeftMenu_Settings', self.mobileBrowser).click()
                dictionary2=['name:General','name:Vehicle Source & Language','Background Sync' ]
                missing = Defect.dictionary_checkifPresent_helper(self, dictionary2, 'Left Menu', True)
                if(missing==''):#i4
                    Element('name:General', self.mobileBrowser).click()
                    if(Element('name:App Version',self.mobileBrowser).isElementPresent()):#i3
                        Element('BackArrow', self.mobileBrowser).click()
                        Element('Settings_VehicleSourceLanguage', self.mobileBrowser).bringToVisibility()
                        Element('Settings_VehicleSourceLanguage', self.mobileBrowser).click()
                        if(Element('name:Language', self.mobileBrowser).isElementPresent()):#i2
                                if(Element('Settings_VehicleSource', self.mobileBrowser).isElementPresent()):#i1
                                    temp = self.lib.fetchDataValue(self.testData,'VehicleSource')
                                    Element('Settings_VehicleSource', self.mobileBrowser).click()
                                    Element('name:'+temp, self.mobileBrowser).click()
                                    success='1'
                                    pass#i1
                                pass#i2
                        pass#i4
                    pass#i5
                if(success==''):
                    #RPORT FAIL
                    self.report.addReportLog('Verifying Left Menu elements and sub elements','Failed to Execute', Status.Fail)
                    pass
                else:
                    #REPORT PASS
                    self.report.addReportLog('Verified Left Menu elements and sub elements','Successfully Executed', Status.Pass)
                    pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
         
    def Copart_ReceivingApp_026(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            searchTypes = ['LotNumber','Make', 'Model', 'Color', 'Year','VIN','Seller', 'SellerType' ]
            missing=''
            for x in searchTypes:
                searchText = self.lib.fetchDataValue(self.testData, x)
                Defect.search_list(self, searchText)
                if(Element('name:'+searchText, self.mobileBrowser).isElementPresent()):
                    pass
                else:
                    missing=missing+','+searchText
                    pass                 
                pass
#             Element('LeftMenu_HomeScreen', self.mobileBrowser).bringToVisibility(True)
            Element('LeftMenu_HomeScreen', self.mobileBrowser).click()
            Defect.get_receivinglist(self)
            Element('Sort_Button', self.mobileBrowser).waitForElement(20)
            Defect.sort_list(self)
            Defect.filter_list(self)
            Defect.click_leftdrawer(self)
            Element('LeftMenu_HomeScreen', self.mobileBrowser).bringToVisibility()
            Element('LeftMenu_HomeScreen', self.mobileBrowser).click()
            if(missing==''):
                if(Element('Receiving_List', self.mobileBrowser).isElementPresent()):
                    self.report.addReportLog('Searched for different input types& navigated to home in the end', 'Successfully Executed', Status.Pass)
                    pass
                pass
            else:
                if(Element('Receiving_List', self.mobileBrowser).isElementPresent()):
                    self.report.addReportLog('Searched for different input types but  missing results for:'+missing, 'Successfully Executed', Status.Pass)
                    pass
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('Searching Text in Receiving list by different input types', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Searching Text in Receiving list by different input types', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        pass
    
    def Copart_ReceivingApp_028(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            searchTypes = ['LotNumber','Make', 'Model', 'Color', 'Year','VIN','Seller', 'SellerType','PlateNum' ]
            missing=''
            for x in searchTypes:
                searchText = self.lib.fetchDataValue(self.testData, x)
                Defect.search_list(self, searchText)
                Element('name:'+searchText, self.mobileBrowser).bringToVisibility()
                if(Element('name:'+searchText, self.mobileBrowser).isElementPresent()):
                    pass
                else:
                    missing=missing+','+searchText
#                     self.report.addReportLog('Searched for different input types but not missing results for:'+missing, 'Successfully Executed', Status.Pass)
                    pass
                if(missing==''):
                    self.report.addReportLog('Searched for different input types& navigated to home in the end', 'Successfully Executed', Status.Pass)
                    pass
                else:
                    self.report.addReportLog('Searched for different input types but not missing results for:'+missing, 'Successfully Executed', Status.Pass)
                    pass
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_029(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Element('BarCodeScanner', self.mobileBrowser).waitForElement(15)
            if(Element('BarCodeScanner', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Validating presence of bar code scanner', 'Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Validating presence of bar code scanner', 'Failed to execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_030(self):
        try:
            searchwrongformat = [self.lib.fetchDataValue(self.testData, 'lotnumberlessthan8'), self.lib.fetchDataValue(self.testData, 'lotnumbergreaterthan8'), self.lib.fetchDataValue(self.testData, 'alphanumeric'), self.lib.fetchDataValue(self.testData, 'specialchars')]
            wrongfm=''
            Defect.login(self)
            Defect.get_receivinglist(self)
            for x in searchwrongformat:
                Element('Search_TextField', self.mobileBrowser).waitForElement(15)
                Element('Search_TextField', self.mobileBrowser).setText(x)
                Element.presskey(self, keycode=66)
                Element('NO_MATCHES_MSG', self.mobileBrowser).waitForElement(6)
                if(Element('NO_MATCHES_MSG', self.mobileBrowser).isElementPresent()):
                    Element('name:OK', self.mobileBrowser).waitForElement(5)
                    Element('name:OK', self.mobileBrowser).click()
                    pass
                else:
                    #FAILED- AS ACCEPTING WRONG FORMAT
                    Element('Search_TextField', self.mobileBrowser).waitForElement(5)
                    if(Element('Search_TextField', self.mobileBrowser).isElementPresent()):
                        wrongfm = wrongfm+','+x
                        pass
                    pass
                pass
            if(wrongfm==''):
                #REPORT PASS
                self.report.addReportLog('Lot Search:NOT ACCEPTING WRONG FORMAT ', 'Successfully Executed', Status.Pass)
                pass
            else:
                #REPORT FAIL
                self.report.addReportLog('Lot Search:ACCEPTING WRONG FORMAT '+wrongfm, 'Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_031(self):
        report=''
        try:
    #         make5alphanum = self.lib.fetchDataValue(self.testData, 'make5alphanum')#AUDI
    #         makegreaterthan5alphanum = self.lib.fetchDataValue(self.testData, 'makegreaterthan5alphanum')#CHEVROLET
    #         model10 = self.lib.fetchDataValue(self.testData, 'model10')#GRAND CARA
    #         modelgreaterthan10 = self.lib.fetchDataValue(self.testData, 'modelgreaterthan10')#GRAND CARAVAN
            searchSplCharMake = self.lib.fetchDataValue(self.testData, 'SearchSplCharMake')#*AUDI
            searchSplCharModel = self.lib.fetchDataValue(self.testData, 'SearchSplCharModel')#*CAMARO
            Defect.login(self)
            Defect.get_receivinglist(self)
            Element('Search_TextField', self.mobileBrowser).waitForElement(15)
    #         make5alphanum= Defect.search_list(self, make5alphanum, True)
    #         makegreaterthan5alphanum = Defect.search_list(self, makegreaterthan5alphanum, True)
    #         model10 = Defect.search_list(self, model10, True)
    #         modelgreaterthan10 = Defect.search_list(self, modelgreaterthan10, True)
            searchSplCharMake = Defect.search_list(self, searchSplCharMake, True)
            searchSplCharModel = Defect.search_list(self, searchSplCharModel, True)
            
    #         if(make5alphanum=='2' or make5alphanum=='3'):
    #             report = 'PROPER MAKE NOT RETURNED'
    #             pass
    #         if(makegreaterthan5alphanum=='1'):
    #             report = report+ 'MAKE GREATER THAN 5 WAS RETURNED. IT SHOULD NOT BE RETURNED,'
    #             pass
    #         if(model10=='2' or model10=='3'):
    #             report = 'PROPER MODEL NOT RETURNED,'
    #             pass
    #         if(modelgreaterthan10=='1'):
    #             report = report+ 'MODEL GREATER THAN 10 WAS RETURNED. IT SHOULD NOT BE RETURNED,'
    #             pass
            if(searchSplCharMake=='1'):
                report = report+'SPECIAL CHARS MAKE SHOULD NOT BE RETURNED'
                pass
            if(searchSplCharModel=='1'):
                report = report+'SPECIAL CHARS MODEL SHOULD NOT BE RETURNED'
                pass
            
            if(report==''):
                self.report.addReportLog('Verified search of make and model with different combinations', 'Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Verifying search of make and model with different combinations.'+report, 'Failed to execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    def Copart_ReceivingApp_032(self):
        try:
            color5chars = self.lib.fetchDataValue(self.testData, 'color5chars')#SILVR
            colormorethan5chars = self.lib.fetchDataValue(self.testData, 'colormorethan5chars')#SILVER
            colorwithsplchars = self.lib.fetchDataValue(self.testData, 'colorwithsplchars')#RED*
            yearlessthan4chars =self.lib.fetchDataValue(self.testData, 'yearlessthan4chars')#03
            year4chars = self.lib.fetchDataValue(self.testData, 'year4chars')
            yearwithsplchars = self.lib.fetchDataValue(self.testData, 'yearwithsplchars')
            
            Defect.login(self)
            Defect.get_receivinglist(self)
            Element('Search_TextField', self.mobileBrowser).waitForElement(15)
            
    #         color5chars = Defect.search_list(self, color5chars, True)#RETURN MUST BE 1
    #         colormorethan5chars = Defect.search_list(self, colormorethan5chars, True)#RETURN MUST BE 2
            colorwithsplchars = Defect.search_list(self, colorwithsplchars, True)#RETURN MUST BE 2
            yearlessthan4chars = Defect.search_list(self, yearlessthan4chars, True)#RETURN MUST BE 2
            year4chars = Defect.search_list(self, year4chars, True)#RETURN MUST BE 1
            yearwithsplchars = Defect.search_list(self, yearwithsplchars, True)#RETURN MUST BE 2
            report=''
    #         if(color5chars!='1'):
    #             report =report+ 'VALID SEARCH for COLOR RETURNED NO RESULTS,'
    #             pass
            if(year4chars!='1'):
                report = report+'VALID SEARCH FOR YEAR RETURNED NO RESULTS,'
                pass
    #         if(colormorethan5chars=='1'):
    #             report = report+'ERROR:COLOR WITH MORE THAN 5 CHARS RETURNED RESULT. IT SHOULD NOT RETURN,'
    #             pass
            if(colorwithsplchars=='1'):
                report = report+'ERROR:COLOR WITH SPL CHARS RETURNED RESULT. IT SHOULD NOT RETURN,'
                pass
            if(yearlessthan4chars=='1'):
                report = report+'ERROR:YEAR WITH LESS THAN 4 CHARS RETURNED RESULT. IT SHOULD NOT RETURN,'
                pass
            if(yearwithsplchars=='1'):
                report = report+'ERROR:YEAR WITH SPL CHARS RETURNED RESULT. IT SHOULD NOT RETURN,'
                pass
            
            if(report==''):
                self.report.addReportLog('COLOR AND YEAR SEARCH VALIDATIONS', 'Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('COLOR AND YEAR SEARCH VALIDATIONS', 'Failed', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
            
    def Copart_ReceivingApp_033(self):
        try:
            #plate = self.lib.fetchDataValue(self.testData, 'plate')#SILVR
            #seller = self.lib.fetchDataValue(self.testData, 'seller')#SILVER
            #sellertype = self.lib.fetchDataValue(self.testData, 'sellertype')#RED*
            platewithsplchars =self.lib.fetchDataValue(self.testData, 'platewithsplchars')#03
            sellertypewithsplchars = self.lib.fetchDataValue(self.testData, 'sellertypewithsplchars')
            sellerwithsplchars = self.lib.fetchDataValue(self.testData, 'sellerwithsplchars')
            
            Defect.login(self)
            Defect.get_receivinglist(self)
            Element('Search_TextField', self.mobileBrowser).waitForElement(15)
            
    #         plate = Defect.search_list(self, plate, True)
    #         seller = Defect.search_list(self, seller, True)#RETURN MUST BE 
    #         sellertype = Defect.search_list(self, sellertype, True)#RETURN MUST BE 
            platewithsplchars = Defect.search_list(self, platewithsplchars, True)#RETURN MUST BE 
            sellerwithsplchars = Defect.search_list(self, sellerwithsplchars, True)#RETURN MUST BE 
            sellertypewithsplchars = Defect.search_list(self, sellertypewithsplchars, True)#RETURN MUST BE 
            report=''
    
            if(platewithsplchars=='1'):
                report = report+'ERROR:PLATES WITH SPL CHARS RETURNED RESULT. IT SHOULD NOT RETURN,'
                pass
            if(sellerwithsplchars=='1'):
                report = report+'ERROR:YEAR WITH LESS THAN 4 CHARS RETURNED RESULT. IT SHOULD NOT RETURN,'
                pass
            if(sellertypewithsplchars=='1'):
                report = report+'ERROR:YEAR WITH SPL CHARS RETURNED RESULT. IT SHOULD NOT RETURN,'
                pass
            
            if(report==''):
                self.report.addReportLog('COLOR AND YEAR SEARCH VALIDATIONS', 'Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('COLOR AND YEAR SEARCH VALIDATIONS', 'Failed', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
#         
#     def 36,39,41, 42,43,44,45,49,87,88,89,90,91,92
    def Copart_ReceivingApp_036(self):
        try:
            missing=''
            dictionary = ['id:top_yearlabel.','id:top_lotlabel.','id:top_vinlabel.','id:top_platenumlabel.','id:top_colorlabel.','id:top_sellerlabel.','id:top_sellertypelabel.']#ADD MORE
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            missing= Defect.dictionary_checkifPresent_helper(self, dictionary, 'step1', True)
            if(missing==''):
                self.report.addReportLog('Checking the lot details on step1', 'Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Checking the lot details on step1. Missing '+missing, 'Failed', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_039(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Element('Sort_Button', self.mobileBrowser).waitForElement(20)
            Element('Sort_Button', self.mobileBrowser).click()
            dictionary = ['Sort_Lot_High2Low','Sort_Lot_Low2High','Sort_Model_A2Z', 'Sort_Model_Z2A','Sort_SellerType_A2Z','Sort_SellerType_Z2A', 'Sort_TripTime_L2O','Sort_TripTime_O2L']#,'Sort_RowLoc_A2Z','Sort_RowLoc_Z2A' ]
            missing=Defect.dictionary_checkifPresent_helper(self, dictionary, 'Receiving Sort', True)
            if(missing==''):
                self.report.addReportLog('Receiving:Tapped on Sort and found options','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Tapped on Sort and found missing options.Missing:'+missing,'Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    def Copart_ReceivingApp_041(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Element('Sort_Button', self.mobileBrowser).waitForElement(20)
            Element('Sort_Button', self.mobileBrowser).bringToVisibility()
            Element('Sort_Button', self.mobileBrowser).click()
            Element('BackArrow', self.mobileBrowser).bringToVisibility()
            Element('BackArrow', self.mobileBrowser).click()
            Element('Search_TextField',self.mobileBrowser).bringToVisibility()
            if(Element('Search_TextField',self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Tapped on Sort and clicked back arrow.Returned to list','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Tapping on sort, clicking back arrow','Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    def Copart_ReceivingApp_042(self):
        try:
            dictionary = ['name:Filter by','BackArrow', 'name:Apply', 'name:Model','name:Seller', 'name:Seller Type','name:Trip Confirmation']
            Defect.login(self)
            Defect.get_receivinglist(self)
            Element('Filter_Button', self.mobileBrowser).waitForElement(15)
            Element('Filter_Button', self.mobileBrowser).bringToVisibility()
            Element('Filter_Button', self.mobileBrowser).click()
            missing=Defect.dictionary_checkifPresent_helper(self, dictionary, 'Images Filter Screen', True)
            if(missing==''):
                self.report.addReportLog('Tapped on Filter on Receiving tab and found all elements','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Tapping on Filter for Receiving tab,missing '+missing,'Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    def Copart_ReceivingApp_043(self):
        #SAME AS 020. REPLACE REC WITH IMAGES LIST
        try:
            tempFilterType = self.lib.fetchDataValue(self.testData, 'FilterByType')
            tempFilterValue = self.lib.fetchDataValue(self.testData, 'FilterByValue')
            Defect.login(self)
            Defect.get_imagedecouplinglist(self)
            Element('Filter_Button', self.mobileBrowser).waitForElement(15)
            Element('Filter_Button', self.mobileBrowser).bringToVisibility()
            Element('Filter_Button', self.mobileBrowser).click()
            dictionary= ['id:Model_Any', 'id:Seller_Any', 'id:SellerType_Any', 'id:TripTime_Any']#PROBLEM HERE THESE IDS DONT EXIST
            missing=Defect.dictionary_checkifPresent_helper(self, dictionary, 'Filter Receiving list', True)
            if(missing!=''):
                Element('name:'+tempFilterType, self.mobileBrowser).bringToVisibility()
                Element('name:'+tempFilterType, self.mobileBrowser).click()
                Element('name:'+tempFilterValue, self.mobileBrowser).bringToVisibility()
                Element('name:'+tempFilterValue, self.mobileBrowser).click()
                Element('name:Apply', self.mobileBrowser).bringToVisibility()
                Element('name:Apply', self.mobileBrowser).click()
                self.report.addReportLog('Tapped on Filter on Receiving tab, Default value is Any. Selected new value for filter','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Tapping on Filter for Receiving tab,missing '+missing,'Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass

    def Copart_ReceivingApp_044(self):
        try:#UPDATE XCEL SHEET TC 44 vs TC21
            tempFilterType = self.lib.fetchDataValue(self.testData, 'FilterByType')
            tempFilterValue = self.lib.fetchDataValue(self.testData, 'FilterByValue')
            Defect.login(self)
            Defect.get_imagedecouplinglist(self)
            Element('Filter_Button', self.mobileBrowser).waitForElement(15)
            Element('Filter_Button', self.mobileBrowser).bringToVisibility()
            Element('Filter_Button', self.mobileBrowser).click()
            Element('name:'+tempFilterType, self.mobileBrowser).bringToVisibility()
            Element('name:'+tempFilterType, self.mobileBrowser).click()
            Element('BackArrow', self.mobileBrowser).bringToVisibility()
            Element('BackArrow', self.mobileBrowser).click()
            Element('name:'+tempFilterType, self.mobileBrowser).bringToVisibility()
            Element('name:'+tempFilterType, self.mobileBrowser).click()
            Element('name:'+tempFilterValue, self.mobileBrowser).bringToVisibility()
            Element('name:'+tempFilterValue, self.mobileBrowser).click()
            Element('name:Apply', self.mobileBrowser).bringToVisibility()
            Element('name:Apply', self.mobileBrowser).click()
            Element('Search_TextField', self.mobileBrowser).waitForElement(10)
            if(Element('Search_TextField', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Tapped on Filter on Images tab, Tap Filter, FilterType. Then Tap Arrow, then Selected new value for filter','Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Trying to tap on Filter on Images tab, Tap Filter, FilterType. Then Tap Arrow, then Selected new value for filter','Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass     

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()