'''
Created on Jun 5, 2015

@author: 175730
'''
import unittest
from org.cognizant.mobile.scripthelper.Utility import Utility
from org.cognizant.mobile.execution.TestReporting import ResultStore, Status,\
    TestReport
from org.cognizant.mobile.testcase.BaseState import BaseState
from org.cognizant.mobile.testcase.Defect import Defect
from org.cognizant.mobile.scripthelper.ObjectRepository import Element

class DefectTAB(unittest.TestCase):


    def setUp(self):
         #print('In setup')
        self.lib = Utility()
        
        print(self.id())
        self.report = ResultStore(self.id(), self.mobileBrowser)
        
        # Invoking BaseState
        baseState = BaseState()
        if (self.testData['BaseState'] == 'PublicUS'):
            baseState.PublicUS(self.mobileBrowser)
            pass
        elif (self.testData['BaseState'] == 'PublicUK'):
            baseState.PublicUK(self.mobileBrowser)
            pass        
        pass        

       


    def tearDown(self):
        self.mobileBrowser.quit()
        TestReport.addToReportList(self.report) 
        pass


    def auto_mma_us_688_tab(self):
        try:
            print("tc")
            #Element('MoreSettings_button_androidTAB', self.mobileBrowser).click()
            Element('Finder_Home_Android', self.mobileBrowser).click()
            Element('name:' + self.testData['QuickPicksCategory'], self.mobileBrowser).waitForElement(3)
            Element('name:' + self.testData['QuickPicksCategory'], self.mobileBrowser).bringToVisibility()
            iCategoryLotCount = self.lib.getQuickPicksCount(self.testData['QuickPicksCategory'], self.mobileBrowser)
            self.report.addReportLog('Checking Count of Lots in Quick Picks: ' + self.testData['QuickPicksCategory'], 'Count of Quick Picks lots: ' + str(iCategoryLotCount), Status.Pass)
            Element('name:' + self.testData['QuickPicksCategory'], self.mobileBrowser).click()
            Element('PageText_Android', self.mobileBrowser).waitForElement(3)  
                      
            if(Element('PageText_Android', self.mobileBrowser).getText().find(str(iCategoryLotCount)) != -1):
                self.report.addReportLog('Checking Count match in Header', 'Vehicle Count for Quick Picks matches with category', Status.Pass)
                pass
            else:
                self.report.addReportLog('Checking Count match in Header', 'Vehicle Count for Quick Picks does not match', Status.Fail)
                pass
                        
            Element('name:&ansflnafnlf',self.mobileBrowser).bringToVisibility()
            
            if(Element('Message_Android', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Message appeared', 'Appeared message: ' + Element('Message_Android', self.mobileBrowser).getText(), Status.Fail)
                Element('Btn_Settings_Android', self.mobileBrowser).click()
            print("tc1") 
            pass
        except AssertionError as ase:
            self.report.addReportLog('Signing in to application', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Signing in to application', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
    
    def auto_mma_uk_657_Public_tab(self):
        try:
            print("tc")
            #Element('MoreSettings_button_androidTAB', self.mobileBrowser).click()
            self.report.addReportLog('Invoke Application', 'event is successful', Status.Pass)
            Element('Page_Up_Android', self.mobileBrowser).click()
            Element('Finder_Home_Android', self.mobileBrowser).waitForElement(2)
            Element('Finder_Home_Android', self.mobileBrowser).click()
            self.report.addReportLog('TAP Vehicle Finder', 'event is successful', Status.Pass)
            Element('TitleText_Android', self.mobileBrowser).waitForElement(3)
            print("tc01") 
            if(Element('Message_Android', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Message appeared', 'Appeared message: ' + Element('Message_Android', self.mobileBrowser).getText(), Status.Fail)
                Element('Btn_Settings_Android', self.mobileBrowser).click()
            print("tc1") 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
    def auto_mma_uk_657_Member_tab(self):
        try:
            print("tc")
            self.lib.signIn(self.mobileBrowser, self.testData)            
            # Checking if user is signed in            
            Element('Sign_Out_Android', self.mobileBrowser).waitForElement(2)
            self.assertTrue(Element('Sign_Out_Android', self.mobileBrowser).isElementPresent(), 'Could not Sign In successfully')
            
            self.report.addReportLog('Signing in to application', 'Signed in to application successfully', Status.Pass)
            
            self.auto_mma_uk_657_Public_tab() 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
  
    def auto_mma_us_656_Public_tab(self):
        try:
            print("tc")
            #Element('MoreSettings_button_androidTAB', self.mobileBrowser).click()
            Element('Finder_Home_Android', self.mobileBrowser).click()
            Element('TitleText_Android', self.mobileBrowser).waitForElement(3)
            
            if(Element('Message_Android', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Message appeared', 'Appeared message: ' + Element('Message_Android', self.mobileBrowser).getText(), Status.Fail)
                Element('Btn_Settings_Android', self.mobileBrowser).click()
            
            print("tc1") 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
    def auto_mma_us_656_Member_tab(self):
        try:
            print("tc")
            self.lib.signIn(self.mobileBrowser, self.testData)            
            # Checking if user is signed in            
            Element('Sign_Out_Android', self.mobileBrowser).waitForElement(2)
            self.assertTrue(Element('Sign_Out_Android', self.mobileBrowser).isElementPresent(), 'Could not Sign In successfully')
            
            self.report.addReportLog('Signing in to application', 'Signed in to application successfully', Status.Pass)
            
            self.auto_mma_us_656_Public_tab() 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass

    def auto_mma_uk_447_Public_tab(self):
        try:
            print("tc")
            #Element('MoreSettings_button_androidTAB', self.mobileBrowser).click()
            self.report.addReportLog('Invoke Application', 'event is successful', Status.Pass)
            Element('Page_Up_Android', self.mobileBrowser).click()
            Element('QuickPicks_Home_Android', self.mobileBrowser).waitForElement(2)
            Element('QuickPicks_Home_Android', self.mobileBrowser).click()
            self.report.addReportLog('TAP Quick Picks', 'event is successful', Status.Pass)
            Element('TitleText_Android', self.mobileBrowser).waitForElement(3)
            print("tc01") 
            if(Element('Message_Android', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Message appeared', 'Appeared message: ' + Element('Message_Android', self.mobileBrowser).getText(), Status.Fail)
                Element('Btn_Settings_Android', self.mobileBrowser).click()
            print("tc1") 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
    def auto_mma_uk_447_Member_tab(self):
        try:
            print("tc")
            self.lib.signIn(self.mobileBrowser, self.testData)            
            # Checking if user is signed in            
            Element('Sign_Out_Android', self.mobileBrowser).waitForElement(2)
            self.assertTrue(Element('Sign_Out_Android', self.mobileBrowser).isElementPresent(), 'Could not Sign In successfully')
            
            self.report.addReportLog('Signing in to application', 'Signed in to application successfully', Status.Pass)
            
            self.auto_mma_uk_447_Public_tab() 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
  
    def auto_mma_us_447_Public_tab(self):
        try:
            print("tc")
            #Element('MoreSettings_button_androidTAB', self.mobileBrowser).click()
            Element('QuickPicks_Home_Android', self.mobileBrowser).click()
            Element('TitleText_Android', self.mobileBrowser).waitForElement(3)
            
            if(Element('Message_Android', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Message appeared', 'Appeared message: ' + Element('Message_Android', self.mobileBrowser).getText(), Status.Fail)
                Element('Btn_Settings_Android', self.mobileBrowser).click()
            
            print("tc1") 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
    def auto_mma_us_447_Member_tab(self):
        try:
            print("tc")
            self.lib.signIn(self.mobileBrowser, self.testData)            
            # Checking if user is signed in            
            Element('Sign_Out_Android', self.mobileBrowser).waitForElement(2)
            self.assertTrue(Element('Sign_Out_Android', self.mobileBrowser).isElementPresent(), 'Could not Sign In successfully')
            
            self.report.addReportLog('Signing in to application', 'Signed in to application successfully', Status.Pass)
            
            self.auto_mma_us_447_Public_tab() 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
    
    
    def auto_mma_uk_390_Public_tab(self):
        try:
            print("tc")
            #Element('MoreSettings_button_androidTAB', self.mobileBrowser).click()
            self.report.addReportLog('Invoke Application', 'event is successful', Status.Pass)
            #Element('Page_Up_Android', self.mobileBrowser).click()
            Element('Finder_Home_Android', self.mobileBrowser).waitForElement(2)
            Element('Finder_Home_Android', self.mobileBrowser).click()
            self.report.addReportLog('TAP Vehicle Finder', 'event is successful', Status.Pass)
            Element('TitleText_Android', self.mobileBrowser).waitForElement(3)
            Element('SearchBtn_Finder_Android', self.mobileBrowser).click()
            Element('PageText_Android', self.mobileBrowser).waitForElement(3)
            print("tc01") 
            if(Element('PageText_Android', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Vehicle Search List', 'Page displayed with ' + Element('PageText_Android', self.mobileBrowser).getText(), Status.Pass)
            else:
                self.report.addReportLog('Vehicle Search List', 'Page did not displayed ' , Status.Fail)
            print("tc1") 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
    def auto_mma_uk_390_Member_tab(self):
        try:
            print("tc")
            self.lib.signIn(self.mobileBrowser, self.testData)            
            # Checking if user is signed in            
            Element('Sign_Out_Android', self.mobileBrowser).waitForElement(2)
            self.assertTrue(Element('Sign_Out_Android', self.mobileBrowser).isElementPresent(), 'Could not Sign In successfully')
            
            self.report.addReportLog('Signing in to application', 'Signed in to application successfully', Status.Pass)
            
            self.auto_mma_uk_390_Public_tab() 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
  
    def auto_mma_us_390_Public_tab(self):
        try:
            print("tc")
            #Element('MoreSettings_button_androidTAB', self.mobileBrowser).click()
            self.report.addReportLog('Invoke Application', 'event is successful', Status.Pass)
            #Element('Page_Up_Android', self.mobileBrowser).click()
            Element('Finder_Home_Android', self.mobileBrowser).waitForElement(2)
            Element('Finder_Home_Android', self.mobileBrowser).click()
            self.report.addReportLog('TAP Vehicle Finder', 'event is successful', Status.Pass)
            Element('TitleText_Android', self.mobileBrowser).waitForElement(3)
            Element('SearchBtn_Finder_Android', self.mobileBrowser).click()
            Element('PageText_Android', self.mobileBrowser).waitForElement(3)
            print("tc01") 
            if(Element('PageText_Android', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Vehicle Search List', 'Page displayed with ' + Element('PageText_Android', self.mobileBrowser).getText(), Status.Pass)
            else:
                self.report.addReportLog('Vehicle Search List', 'Page did not displayed ' , Status.Fail)
            print("tc1") 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
    def auto_mma_us_390_Member_tab(self):
        try:
            print("tc")
            self.lib.signIn(self.mobileBrowser, self.testData)            
            # Checking if user is signed in            
            Element('Sign_Out_Android', self.mobileBrowser).waitForElement(2)
            self.assertTrue(Element('Sign_Out_Android', self.mobileBrowser).isElementPresent(), 'Could not Sign In successfully')
            
            self.report.addReportLog('Signing in to application', 'Signed in to application successfully', Status.Pass)
            
            self.auto_mma_us_390_Public_tab() 
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC Execution', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC Execution', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
        pass
    
   
    def auto_mma_uk_378_Public_tab(self):
        self.auto_mma_uk_390_Public_tab() 
        pass
    def auto_mma_uk_378_Member_tab(self):
        self.auto_mma_uk_390_Member_tab()
        pass
  
    def auto_mma_us_378_Public_tab(self):
        self.auto_mma_us_390_Public_tab()
        pass
        
    def auto_mma_us_378_Member_tab(self):
        self.auto_mma_us_390_Member_tab()
        pass
    def auto_mma_uk_194_Public_tab(self):
        self.auto_mma_uk_390_Public_tab() 
        pass
    def auto_mma_uk_194_Member_tab(self):
        self.auto_mma_uk_390_Member_tab()
        pass
  
    def auto_mma_us_194_Public_tab(self):
        self.auto_mma_us_390_Public_tab()
        pass
        
    def auto_mma_us_194_Member_tab(self):
        self.auto_mma_us_390_Member_tab()
        pass
    def auto_mma_us_707_Member_tab(self):
        defect=Defect()
        defect.report=self.report
        defect.lib=self.lib
        defect.mobileBrowser=self.mobileBrowser
        defect.testData=self.testData
        defect.auto_mma_us_707()
        
        pass

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()