'''
Created on May 19, 2015

@author: 209537
'''
import unittest
from enum import Enum
import time
from org.cognizant.mobile.scripthelper.Utility import Utility
from org.cognizant.mobile.execution.TestReporting import ResultStore, Status, TestReport
from org.cognizant.mobile.testcase.BaseState import BaseState
from org.cognizant.mobile.scripthelper.ObjectRepository import Element
import random
from datetime import datetime
from time import sleep
from selenium.webdriver import android
from selenium.webdriver.opera.options import AndroidOptions


class Defect(unittest.TestCase):


    def setUp(self):
        #print('In setup')
        self.lib = Utility()
        
        print(self.id())
        self.report = ResultStore(self.id(), self.mobileBrowser)
        
        self.StartTime = datetime.now()
        
        # Invoking BaseState
#         baseState = BaseState()
#         if (self.testData['BaseState'] == 'PublicUS'):
#             baseState.PublicUS(self.mobileBrowser)
#             pass
#         elif (self.testData['BaseState'] == 'PublicUK'):
#             baseState.PublicUK(self.mobileBrowser)
#             pass        
#         pass        

    def tearDown(self):
        self.mobileBrowser.quit()
        self.EndTime = datetime.now()
        delta = self.EndTime - self.StartTime
        self.report.setExecutionTime(delta)
        TestReport.addToReportList(self.report)    
        pass
    
    # Place Holder
    
    #===========================================================================
    # START-**********Functions for various flows**************
    #===========================================================================
    def login(self):
        try:
            Element('Login_Username', self.mobileBrowser).waitForElement(5)
            Element('Login_Username', self.mobileBrowser).setText(self.lib.fetchDataValue(self.testData, 'UserName'))
            Element('Login_Password', self.mobileBrowser).waitForElement(5)
            Element('Login_Password', self.mobileBrowser).setText(self.lib.fetchDataValue(self.testData, 'Password'))
            Element('Login_SignIn_Btn', self.mobileBrowser).waitForElement(5)
            Element('Login_SignIn_Btn', self.mobileBrowser).click()   
            Element('Receiving_List', self.mobileBrowser).waitForElement(15)
            if(Element('Receiving_List', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog("Login", "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('Login', 'Failed to login', Status.Fail)
                pass 
        except AssertionError as ase:
            self.report.addReportLog('Login Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Login Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    pass
    
    def click_leftdrawer(self):
        try:
            Element('Login_Left_Menu_Btn', self.mobileBrowser).bringToVisibility()
            Element('Login_Left_Menu_Btn', self.mobileBrowser).click()
            self.report.addReportLog("Click Left Drawer", "Successfully executed", Status.Pass)
        except AssertionError as ase:
            self.report.addReportLog('Click Left Drawer Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Click Left Drawer Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    pass
    
    def click_rightdrawer(self):
        try:
#             Element('Right_Drawer_Menu', self.mobileBrowser).waitForElement(6)
            Element('Right_Drawer_Menu', self.mobileBrowser).bringToVisibility()
            Element('Right_Drawer_Menu', self.mobileBrowser).click()
            self.report.addReportLog("Click Right Drawer", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Click Right Drawer Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Click Right Drawer Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    def get_receivinglist(self):
        try:
#             Element('Receiving_List', self.mobileBrowser).waitForElement(6)
            Element('Receiving_List', self.mobileBrowser).waitForElement(15)
            Element('Receiving_List', self.mobileBrowser).click()
            self.report.addReportLog("Get Receiving List", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Getting Receiving List from Home Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Getting Receiving List from Home Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def get_receivinglist_for_submittingWithoutImages(self):
        try:
            Element('Home_Yard', self.mobileBrowser).waitForElement(5)
            Element('Home_Yard', self.mobileBrowser).click()
            Element('Yard12', self.mobileBrowser).waitForElement(6)
            Element('Yard12', self.mobileBrowser).click()
            Element('Receiving_List', self.mobileBrowser).waitForElement(15)
            Element('Receiving_List', self.mobileBrowser).click()
            self.report.addReportLog("Get Receiving List", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Getting Receiving List from Home Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Getting Receiving List from Home Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def select_lot(self, i=0):
        try:
            lotnum = 'Lot_'+str(i)
            Element(lotnum, self.mobileBrowser).waitForElement(15)
            Element(lotnum,self.mobileBrowser).bringToVisibility()
            Element(lotnum, self.mobileBrowser).click()
            if(Element('VIN_Toggle_No', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog("Lot Selected from List", "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('Selecting a lot from list Failed', 'Failed', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('Selecting a lot from list Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Selecting a lot from list Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    def select_imaging_lot(self, i=0):
        try:
            lotnum = 'Lot_Images_'+str(i)
            Element(lotnum, self.mobileBrowser).waitForElement(15)
            Element(lotnum,self.mobileBrowser).bringToVisibility()
            Element(lotnum, self.mobileBrowser).click()
            if(Element('VIN_Toggle_No', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog("Lot Selected from List", "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('Selecting a lot from list Failed', 'Failed', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('Selecting a lot from list Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Selecting a lot from list Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    def lot_loop(self, i=0):
        try:
            lotnum = 'Lot_'+str(i)
            Element(lotnum,self.mobileBrowser).bringToVisibility()
            if(i==0):
                Element(lotnum, self.mobileBrowser).waitForElement(15)
                pass
            
            Element(lotnum, self.mobileBrowser).click()
            iStringVal = str(i)
            Defect.step1_vin_screen(self,iStringVal)
            Defect.step2_basic_info(self,iStringVal)
            Defect.step3_run_condition(self, iStringVal)
            Defect.step4_seller_required_fields(self,iStringVal)
            Defect.step5_service_orders(self, iStringVal)
            Defect.step6_images(self)
            self.report.addReportLog("Executed all 6 steps for Lot "+i, "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Executing Lot '+lotnum+' in loop from list Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Executing Lot '+lotnum+' in loop from list Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    def step1_vin_screen(self,i='0'):
        try:
            mylotid="top_lotval."
            ltval=Element('id:'+mylotid, self.mobileBrowser).getText()
            print("LOT NBR:"+ltval) 
            vin_toggle= 'VIN_Toggle_Lot'+i
            vin_toggle= self.lib.fetchDataValue(self.testData, vin_toggle)
            Element(vin_toggle, self.mobileBrowser).waitForElement(7)
            Element(vin_toggle, self.mobileBrowser).click()
            if(vin_toggle!='VIN_Toggle_No'):
                vin_testdata ='VIN_Textfield_Lot'+i
                Element('VIN_Textfield',self.mobileBrowser).bringToVisibility()
                Element('VIN_Textfield', self.mobileBrowser).setTextAndNumForVin(self.lib.fetchDataValue(self.testData, vin_testdata))
                if(Element('id:top_vinval.',self.mobileBrowser).isElementPresent()):
                    Element('VIN_Mismatch_Alert',self.mobileBrowser).bringToVisibility()
                    Element('VIN_Mismatch_Alert',self.mobileBrowser).click()
                    pass
                Element('ConfirmVIN_Textfield',self.mobileBrowser).bringToVisibility()
                Element('ConfirmVIN_Textfield', self.mobileBrowser).setTextAndNumForVin(self.lib.fetchDataValue(self.testData, vin_testdata))
                pass
            else:
                pass
            Element('VIN_Submit', self.mobileBrowser).waitForElement(5)
            Element('VIN_Submit', self.mobileBrowser).click()
            self.report.addReportLog("Step 1 for Lot "+i, "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Step1 VIN Screen Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Step1 VIN Screen Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def step2_basic_info(self,i='0',submit= True):
        try:
            temp ='VehicleType_Lot'+i
            temp=self.lib.fetchDataValue(self.testData, temp)
            Element('VehicleType_Finder', self.mobileBrowser).bringToVisibility()
            Element('VehicleType_Finder', self.mobileBrowser).click() 
             
            Element('name:' + temp, self.mobileBrowser).bringToVisibility()
            Element('name:' + temp, self.mobileBrowser).click()
            #YEAR
            temp='Year_Lot'+i
            temp=self.lib.fetchDataValue(self.testData, temp)
            Element('Year_Step2', self.mobileBrowser).click()  
            Element('name:' + temp, self.mobileBrowser).bringToVisibility()
            Element('name:' + temp, self.mobileBrowser).click()
            #MAKE
            temp = 'Make_Lot'+i
            temp=self.lib.fetchDataValue(self.testData, temp)
            Element('Make_Step2', self.mobileBrowser).click()  
            Element('name:' + temp, self.mobileBrowser).bringToVisibility()
            Element('name:' + temp, self.mobileBrowser).click()
            #MODEL
            temp = 'Model_Lot'+i
            temp=self.lib.fetchDataValue(self.testData, temp)
            Element('Model_Step2', self.mobileBrowser).click()  
            Element('name:' + temp, self.mobileBrowser).bringToVisibility()
            Element('name:' + temp, self.mobileBrowser).click()
            #COLOR
            temp='Color_Lot'+i #COUNTRYCODE
            temp=self.lib.fetchDataValue(self.testData, temp)
            Element('Color_Step2', self.mobileBrowser).click()  
            Element('name:' + temp, self.mobileBrowser).bringToVisibility()
            Element('name:' + temp, self.mobileBrowser).click()
            #HAS TRANSMISSION
            temp = 'HasTransmission_Lot'+i
            temp=self.lib.fetchDataValue(self.testData, temp)
            Element(temp, self.mobileBrowser).bringToVisibility()
            Element(temp, self.mobileBrowser).click()
            #HAS ENGINE
            temp = 'HasEngine_Lot'+i
            temp=self.lib.fetchDataValue(self.testData, temp)
            Element(temp,self.mobileBrowser).bringToVisibility()
            Element(temp,self.mobileBrowser).click()
            
            #HAS KEYS
            temp='HasKeys_Lot'+i
            temp=self.lib.fetchDataValue(self.testData, temp)
            Element(temp,self.mobileBrowser).bringToVisibility()
            Element(temp,self.mobileBrowser).click()
            #ODOMETER
            determineOdom = 'Odometer_Lot'+i
            determineOdomValue = self.lib.fetchDataValue(self.testData, determineOdom)
            if(determineOdomValue != 'NONE'):
                Element('Odometer_Reading', self.mobileBrowser).bringToVisibility()
                Element('Odometer_Reading', self.mobileBrowser).setNumeric(determineOdomValue)
                pass
            else:                 
                tempOdom = 'OdometerButton_Lot' + i
                tempOdom = self.lib.fetchDataValue(self.testData, tempOdom)
                Element(tempOdom, self.mobileBrowser).bringToVisibility()
                Element(tempOdom, self.mobileBrowser).click()
                pass
            
            #HAS PLATES
            temp = 'HasPlates_Lot'+i
            temp=self.lib.fetchDataValue(self.testData, temp)
            Element(temp,self.mobileBrowser).bringToVisibility()
            Element(temp,self.mobileBrowser).click()
             
            #NO. OF PLATES
            if(temp=='HasPlates_Yes'):
                tempv = 'NoOfPlates_Lot'+i
                tempv=self.lib.fetchDataValue(self.testData, tempv)
                Element(tempv,self.mobileBrowser).bringToVisibility()
                Element(tempv,self.mobileBrowser).click()       
            #LICENSE PLATE
                temp = 'LicensePlate_Lot'+i
                temp = self.lib.fetchDataValue(self.testData, temp)
            #MONTH
                temp = 'Month_Lot'+i
                temp = self.lib.fetchDataValue(self.testData, temp)
                Element('Month',self.mobileBrowser).bringToVisibility()
                Element('Month',self.mobileBrowser).click()
                Element('name:'+temp, self.mobileBrowser).bringToVisibility()
                Element('name:'+temp, self.mobileBrowser).click()
            #YEAR
                temp = 'LP_Year_Lot'+i
                temp = self.lib.fetchDataValue(self.testData, temp)
                Element('Year_LP',self.mobileBrowser).bringToVisibility()
                Element('Year_LP',self.mobileBrowser).click()
                Element('name:'+temp, self.mobileBrowser).bringToVisibility()
                Element('name:'+temp, self.mobileBrowser).click()
            #STATE
                temp = 'State_Lot'+i
                temp = self.lib.fetchDataValue(self.testData, temp)
                Element('State',self.mobileBrowser).bringToVisibility()
                Element('State',self.mobileBrowser).click()
                Element('name:'+temp, self.mobileBrowser).bringToVisibility()
                Element('name:'+temp, self.mobileBrowser).click()
                pass
        
            else:
                pass
             
            #PRIMARY DAMAGE
            temp = 'PrimaryDamage_Lot'+i
            temp=self.lib.fetchDataValue(self.testData, temp)
            Element('PrimDamage', self.mobileBrowser).bringToVisibility()
            Element('PrimDamage', self.mobileBrowser).click()  
            Element('name:' + temp, self.mobileBrowser).bringToVisibility()
            Element('name:' + temp, self.mobileBrowser).click()
            #SECONDARY DAMAGE
            temp = 'SecondaryDamage_Lot'+i
            temp=self.lib.fetchDataValue(self.testData, temp)
            Element('SecDamage', self.mobileBrowser).bringToVisibility()
            Element('SecDamage', self.mobileBrowser).click()  
            Element('name:' + temp, self.mobileBrowser).bringToVisibility()
            Element('name:' + temp, self.mobileBrowser).click()
            if(submit== False):
                self.report.addReportLog("Step 2 for Lot without submitting", "Successfully executed", Status.Pass)
                pass
            #SAVE 
            if(submit==True):
                Element('SaveAndContinue', self.mobileBrowser).bringToVisibility() 
                Element('SaveAndContinue', self.mobileBrowser).click()
                self.report.addReportLog("Step 2 for Lot "+i, "Successfully executed", Status.Pass)
            pass
        #         Defect.step3_run_condition(self)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Step2 Basic Information Screen Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Step2 Basic Information Screen Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def step3_run_condition(self, i='0'):
        try:
            temp = 'RunCondition_Lot'+i
            temp = self.lib.fetchDataValue(self.testData, temp)
            Element(temp, self.mobileBrowser).waitForElement(7)
            Element(temp, self.mobileBrowser).click()
            Element('RunCondition_Save', self.mobileBrowser).bringToVisibility()
            Element('RunCondition_Save', self.mobileBrowser).click()
            self.report.addReportLog("Step 3 for Lot "+i, "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Step3 Run Condition Screen Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Step3 Run Condition Screen Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def step4_seller_required_fields(self, i=0, skipselections=False): 
        try:
            if(skipselections==False):
                srCount=1
                LotIdentity = 'Lot_'+str(i)+'_'
                pick = LotIdentity+'togglecountforSR'
                srCount = self.lib.fetchDataValue(self.testData, pick)
                nooToggles = int(srCount)
                for j in range(0, nooToggles):
                    temp = LotIdentity+'SellerReq_'+str(j)
                    temp = self.lib.fetchDataValue(self.testData, temp)
                    Element('id:'+temp, self.mobileBrowser).bringToVisibility()
                    Element('id:'+temp, self.mobileBrowser).click()
                pass
            pass
            Element('SellerReq_SaveNProceed', self.mobileBrowser).bringToVisibility()
            Element('SellerReq_SaveNProceed', self.mobileBrowser).click()
            
            Element('STEP5_SUBTITLE', self.mobileBrowser).bringToVisibility()
            if(Element('STEP5_SUBTITLE', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog("Step 4 for Lot "+str(i), "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('Step4 Seller Required Fields Screen Submission Failed', 'Failed', Status.Fail)
                pass
        except AssertionError as ase:
            self.report.addReportLog('Step4 Seller Required Fields Screen Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Step4 Seller Required Fields Screen Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def step5_service_orders(self, i='0'):
        try:
            Element('SuggestOrder_Select', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_Select', self.mobileBrowser).click()
            Element('SuggestOrder_WashNVaccum', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_WashNVaccum', self.mobileBrowser).click()
            Element('SuggestOrder_SaveNGo', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_SaveNGo', self.mobileBrowser).click()
    #         Element('SuggestOrder_SubmitNoImages', self.mobileBrowser).bringToVisibility()
    #         Element('SuggestOrder_SubmitNoImages', self.mobileBrowser).click()
            Element('id:ServiceOrder_Continue.', self.mobileBrowser).bringToVisibility()
            Element('id:ServiceOrder_Continue.', self.mobileBrowser).click()
            self.report.addReportLog("Step 5 for Lot "+i, "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Step5 Service Orders Screen Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Step5 Service Orders Screen Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def submit_without_images(self):
        try:
            Element('Proceed_With_Lot_Submission', self.mobileBrowser).bringToVisibility()
            Element('Proceed_With_Lot_Submission', self.mobileBrowser).click()
            self.report.addReportLog("Submitting without Images for Lot", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Submitting without images on Step5 Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Submitting without images on Step5 Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def step_5_submit_without_images(self):
        Element('SuggestOrder_Select', self.mobileBrowser).waitForElement(8)
        Element('SuggestOrder_Select', self.mobileBrowser).click()
        Element('SuggestOrder_WashNVaccum', self.mobileBrowser).bringToVisibility()
        Element('SuggestOrder_WashNVaccum', self.mobileBrowser).click()
        Element('SuggestOrder_SaveNGo', self.mobileBrowser).bringToVisibility()
        Element('SuggestOrder_SaveNGo', self.mobileBrowser).click()
        Element('SuggestOrder_SubmitNoImages', self.mobileBrowser).bringToVisibility()
        Element('SuggestOrder_SubmitNoImages', self.mobileBrowser).click()
        pass
            
    
    def step6_images(self, submit=True):
        try:
            images = ['left_1.', 'right_1.', 'left_2.','right_2.', 'left_3.', 'right_3.', 'left_4.', 'right_4.','left_5.', 'right_5.']
            for x in images:
                    Element('id:'+x, self.mobileBrowser).bringToVisibility()
                    Element('id:'+x, self.mobileBrowser).click()
                    time.sleep(3)
                    Element.presskey(self,keycode=24)
                    time.sleep(2)
                    Element('name:OK', self.mobileBrowser).bringToVisibility(False,True)
                    if(Element('name:OK', self.mobileBrowser).isElementPresent()):
                        Element('name:OK', self.mobileBrowser).click()
                        pass
                    else:
                        Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).bringToVisibility()
                        Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).click()
                        pass
                        
                    time.sleep(1)
                    pass
            if(submit):
                Element('id:SaveAndComplete.', self.mobileBrowser).bringToVisibility()
                Element('id:SaveAndComplete.', self.mobileBrowser).click()
                Element('id:android:id/button1', self.mobileBrowser).bringToVisibility()
                Element('id:android:id/button1', self.mobileBrowser).click()
                self.report.addReportLog("Step 6 for Lot and submission", "Successfully executed", Status.Pass)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('Step6 Pictures Screen Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Step6 Pictures Screen Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def take_image(self):
        try:
            Element('className:android.view.View', self.mobileBrowser).bringToVisibility()
            Element('className:android.view.View', self.mobileBrowser).click()
            Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).bringToVisibility()
            Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).click()
            self.report.addReportLog("Taking images for Lot ", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Taking Pictures Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Taking Pictures Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def get_imagedecouplinglist(self):
        try:
#             Element('Images_Decoup', self.mobileBrowser).waitForElement(5)
#             Element('Images_Decoup',self.mobileBrowser).bringToVisibility(False, True)
            if(Element('Images_Decoup',self.mobileBrowser).isElementPresent()):
                Element('Images_Decoup',self.mobileBrowser).click()
                pass
            else:
                Element('Home_Yard', self.mobileBrowser).waitForElement(5)
                Element('Home_Yard', self.mobileBrowser).click()
#                 temp = self.lib.fetchDataValue(self.testData,'YardSearch')
#                 if(temp==''):
#                     temp='12'
#                     pass
                Element('Yard12', self.mobileBrowser).waitForElement(5)
                Element('Yard12', self.mobileBrowser).click()
                Element('Images_Decoup',self.mobileBrowser).bringToVisibility()
                Element('Images_Decoup',self.mobileBrowser).click()
                pass
                
            self.report.addReportLog("Get Image Decoupling List from Home", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Getting Images List from Home Screen Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Getting Images List from Home Screen Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def sort_list(self):
        try:
    #         SortingList = ['Lot Number - Low to High','Lot Number - High to Low','Model - A to Z','Model - Z to A','Seller - A to Z','Seller - Z to A','Seller Type - A to Z','Seller Type - Z to A','Trip Time - Latest to Oldest',
    # 'Trip Time - Oldest to Latest','Row Location- A to Z','Row Location- Z to A']
            tempsort = self.lib.fetchDataValue(self.testData, 'Sort')
            Element('Sort_Button', self.mobileBrowser).bringToVisibility()
            Element('Sort_Button', self.mobileBrowser).click()
            print('SORTING ORDER IS:'+tempsort)
            Element(tempsort, self.mobileBrowser).bringToVisibility()
            Element(tempsort, self.mobileBrowser).click()
            self.report.addReportLog("Sorting List", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Sorting  List Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Sorting  List Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    def filter_list(self,i=''):
        try:
            tempFilterType = self.lib.fetchDataValue(self.testData, 'FilterByType'+i)
            print('tempFilterType:'+tempFilterType)
            tempFilterValue = self.lib.fetchDataValue(self.testData, 'FilterByValue'+i)
            print('FilterByValue:'+tempFilterValue)
            Element('Filter_Button', self.mobileBrowser).bringToVisibility()
            Element('Filter_Button', self.mobileBrowser).click()
            Element('name:'+tempFilterType,self.mobileBrowser).bringToVisibility()
            Element('name:'+tempFilterType,self.mobileBrowser).click()
            Element('name:'+tempFilterValue,self.mobileBrowser).bringToVisibility()
            Element('name:'+tempFilterValue,self.mobileBrowser).click()
            if(i==''):
                Element('name:Apply',self.mobileBrowser).bringToVisibility()
                Element('name:Apply',self.mobileBrowser).click()
                self.report.addReportLog("Filtering List", "Successfully executed", Status.Pass)
            pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('Filtering  List Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Filtering  List Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def search_list(self, searchText, status=False):
        try:
            print('searching '+searchText)
            Element('Search_TextField', self.mobileBrowser).waitForElement(15)
            Element('Search_TextField', self.mobileBrowser).bringToVisibility()
            Element('Search_TextField', self.mobileBrowser).setText(searchText)
            Element.presskey(self, keycode=66)
            sleep(3)
            
            if(Element('name:There are no matches',self.mobileBrowser).isElementPresent()):
                print('THERE ARE NO MATCHES MSG')
                Element('name:OK', self.mobileBrowser).bringToVisibility()
                Element('name:OK', self.mobileBrowser).click()
                if(status==True):
                    print('returning 2')
                    self.report.addReportLog("Searching from List. No Results returned for search:"+searchText, "Successfully executed", Status.Pass)
                    return '2'
                pass
            elif(Element('name:'+searchText, self.mobileBrowser).isElementPresent()):
                print('FOUND THE ELEMENT')
                print('retrieved '+searchText)
                self.report.addReportLog("Searching from List. Results returned for search:"+searchText, "Successfully executed", Status.Pass)
                if(status==True):
                    return "1"
                pass
            else:
                if(Element('name:OK', self.mobileBrowser).isElementPresent()):
                    Element('name:OK', self.mobileBrowser).bringToVisibility()
                    Element('name:OK', self.mobileBrowser).click()
                    pass
                if(status==True):
                    self.report.addReportLog("Searching from List. No Results returned for search:"+searchText, "Fail", Status.Fail)
                    return '3'
                pass
            pass
                
                
            
                
#             if(Element('name:'+searchText, self.mobileBrowser).isElementPresent()):
#                 print('retrieved '+searchText)
#                 self.report.addReportLog("Searching from List. Results returned for search:"+searchText, "Successfully executed", Status.Pass)
#                 if(status==True):
#                     return "1"
#                 pass
#                 pass
#                 
#             elif(Element('name:OK', self.mobileBrowser).isElementPresent()):
#                 print('no matches for  '+searchText)
#                 self.report.addReportLog("Searching from List. No Results returned for search:"+searchText, "Successfully executed", Status.Pass)
#                 if(status==True):
#                     Element('name:OK', self.mobileBrowser).bringToVisibility()
#                     Element('name:OK', self.mobileBrowser).click()
#                     return "2"
#                 pass
#                 pass
#             else:
#                 if(Element('name:There are no matches', self.mobileBrowser).isElementPresent()):
#                     Element('name:OK', self.mobileBrowser).bringToVisibility()
#                     Element('name:OK', self.mobileBrowser).click()
#                     pass  
#                 self.report.addReportLog("Searching from List", "failed to execute", Status.Fail)
#                 if(status==True):
#                     return "3"
#                 pass
#                 pass
#                 
            pass
        except AssertionError as ase:
            self.report.addReportLog('Searching From  List Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Searching From  List Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def imagedecoupling_execute(self, i='0'):
        try:
            tempvar = 'ImagesDecoupling_Lot'+i
            Element(tempvar, self.mobileBrowser).waitForElement(15)
            Element(tempvar, self.mobileBrowser).bringToVisibility()
            Element(tempvar, self.mobileBrowser).click()
            Defect.step6_images(self)
            self.report.addReportLog("Image Decoupling", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Image Decoupling Flow Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Image Decoupling Flow Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
        
    def Receiving_Flow(self, countyryCode="US"):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            execLots = self.lib.fetchDataValue(self.testData, 'ExecuteNoofLots')
            print(execLots)
            for i in range(0, int(execLots)):
                    print(i)
                    Defect.lot_loop(self, i)
            pass
            self.report.addReportLog("Receiving Flow", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Receiving Flow Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Receiving Flow Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    
    def ImageDecoupling_Flow(self):
        try:
            execLotsID = self.lib.fetchDataValue(self.testData, 'ExecuteNoofLotsImageDecoupling')
            Defect.get_imagedecouplinglist(self)
            for k in range(0, int(execLotsID)):
                Defect.imagedecoupling_execute(self, str(k))
            pass
            self.report.addReportLog("Image Decoupling Flow", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Image Decoupling Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Image Decoupling Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
                    
    def sign_out(self):
        try:
            Defect.click_leftdrawer(self)
            Element('Sign_Out',self.mobileBrowser).bringToVisibility()
            Element('Sign_Out',self.mobileBrowser).click() 
            Element('Confirm_Sign_Out',self.mobileBrowser).bringToVisibility()
            Element('Confirm_Sign_Out',self.mobileBrowser).click()
            self.report.addReportLog("Sign Out", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Sign Out Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Sign Out Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    def login_helper(self, uName, password):
        Element('Login_Username', self.mobileBrowser).waitForElement(5)
        Element('Login_Username', self.mobileBrowser).setText(uName)
        Element('Login_Password', self.mobileBrowser).waitForElement(5)
        Element('Login_Password', self.mobileBrowser).setText(password)
        Element('Login_SignIn_Btn', self.mobileBrowser).waitForElement(5)
        Element('Login_SignIn_Btn', self.mobileBrowser).click()
        pass 
           
    def checkifPresent_helper(self, elementid,element, clickElement='false'):
        if(Element(elementid, self.mobileBrowser).isElementPresent()):
            if(clickElement!='false'):
                Element(elementid, self.mobileBrowser).click() 
            pass
        else:
            self.report.addReportLog(element+' is missing', 'Test Case failed as the element is not present', Status.Fail)
            pass
        pass
    
    def dictionary_checkifPresent_helper(self, dictionary, screenName, valuereturn=False):
        found=''
        missing = ''
        for x in dictionary:
            Element(x, self.mobileBrowser).bringToVisibility()
            if(Element(x, self.mobileBrowser).isElementPresent()):
                found=found+','+x
                print('dictionary_checkifPresent_helper found:'+found)
                pass
            else:
                missing = missing+','+x
                print('dictionary_checkifPresent_helper missing:'+missing)
                pass
            pass
        if(valuereturn==False):
            if(missing==''):
                self.report.addReportLog('Found elements on screen:'+screenName+'the elements are:'+found, 'Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('On Screen:'+screenName+' the missing elements are:'+missing, 'Test Case failed as the element is not present', Status.Fail)
                pass
        else:
            return missing
        pass
            
    def navigateToStep_helper(self, stepnumber):
        step= 'Step'+stepnumber
        Defect.click_rightdrawer(self)
        Element(step,self.mobileBrowser).bringToVisibility()
        Element(step,self.mobileBrowser).click()
        pass
         
         
         
    #===========================================================================
    # ******************END FUNCTIONS **************************
    #===========================================================================
    
    
    
    #===========================================================================
    # START- TESTCASES
    #===========================================================================
    def END_TO_END(self):
        try:
            starttime = time.time()
            print('Executing END_TO_END')
            Defect.Receiving_Flow(self)
            Defect.click_leftdrawer(self)
            Element('LeftMenu_HomeScreen', self.mobileBrowser).bringToVisibility()
            Element('LeftMenu_HomeScreen', self.mobileBrowser).click()
    #         Defect.ImageDecoupling_Flow(self)
            Defect.sign_out(self)
            endtime = time.time()
            totaltime = endtime-starttime
            print('RAN THE TEST CASE IN :'+str(totaltime))
            self.report.addReportLog("END_TO_END", "Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('END TO END TC Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('END TO END TC Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    #PCH
    
    def BuildDriver(self):
        k=0
        tcses = ['03','04','05','06','10','11','12','13','16','17','18','19']#,'20','21','22','24','25','26','28','29','30','31','32','33','36','39','41','42','43','44','46','47','50']
        for y in tcses:
            for x in range(1,2):
                k=k+1
                r1start = '<Row_ID id="'
                r1end = '">'
                tcstart = '<TC_ID>Copart_ReceivingApp_0'
                tcend='</TC_ID>'
                dsc = '<Description></Description>'
                modstart = '<Module>'
                modend = '</Module>'
                ex  = '<Execute>Yes</Execute>'
                dev = '<Device>Android_00'+str(x)+'</Device>'
                r2end = '</Row_ID>'
                print(r1start+str(k)+r1end)
                print(tcstart+y+tcend)
                print(dsc)
                if(int(y)<45):
                    print('<Module>Login_Home</Module>')
                    pass
                elif(int(y)>45 and int(y)<63):
                    print('<Module>Step1_Step2</Module>')
                    pass
                elif(int(y)>62 and int(y)<73):
                    print('<Module>Step3_Step4</Module>')
                    pass
                else:
                    print('<Module>Step5_Step6</Module>')
                    pass
                print(ex)
                print(dev)
                print(r2end)
#                     pass
                pass
            pass
        pass
  
    #===========================================================================
    # TEST CASES FROM QA
    #===========================================================================
    
    class Messages(Enum):
        INCORRECT_USERNAME_OR_PASSWORD = 'Error: Username or Password is invalid'
        USERNAME_PASSWORD_REQUIRED='Error: Username and Password are required for Login'
        INCORRECT_PASSWORD_ATTEMPTS = 'You have entered incorrect password twice. Entering an incorrect password this time will lock the Username'
        STEP1_SUBTITLE='VIN: Step 1 of 6'
        STEP2_SUBTITLE='Basic Information: Step 2 of 6'
        STEP3_SUBTITLE= 'Run Condition: Step 3 of 6'
        STEP4_SUBTITLE='Seller Required Fields: Step 4 of 6'
        STEP5_SUBTITLE='Service Orders: Step 5 of 6'
        STEP6_SUBTITLE='Pictures: Step 6 of 6'
        STEP1_ENTERVIN_TOPROCEED='To proceed, please enter the VIN'
        PICTURES_HEADER_MESSAGE= 'Please take images of the vehicle to record its condition. All images must be taken before continuing. Tap examples below to retake shots.'
        THERE_ARE_NO_MATCHES = 'There are no matches'
        VIN_MISMATCH_ERROR='VIN mismatch. Please re-enter VIN in Confirm VIN Text Box'
        VIN_CONFIRMVIN_MISMATCH='VIN Confirm mismatch. Please re-confirm VIN'
        PICTURES_SCREEN_HELPER_MSG = 'Please take images of the vehicle to record its condition. All images must be taken before continuing. Tap examples below to retake shots.'
        PROCEED_WITH_LOT_SUBMISSION='Proceed with the lot data submission?'
        CAPTURE_ALL_IMAGES_ERROR='All images required for receiving have not been captured'
        pass
    
    class Pictures(Enum):
        PIC_1='Passenger side front angle'
        PIC_2='Driver side front angle'
        PIC_3='Driver side rear angle'
        PIC_4='Passenger side rear angle'
        PIC_5='Passenger front interior'
        PIC_6='Passenger rear interior'
        PIC_7='Open hood engine front'
        PIC_8='Odometer'
        PIC_9='Damage/Feature #1'
        PIC_10='Damage/Feature #2'
        APPROVE_BUTTON_ID = 'id:com.motorola.camera:id/review_approve'
        CANCEL_BUTTON_ID = 'id:com.motorola.camera:id/review_cancel'
        pass

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()