'''
Created on Apr 30, 2015

@author: 209537
'''
import unittest

from org.cognizant.mobile.execution.TestReporting import ResultStore, Status, TestReport
from org.cognizant.mobile.scripthelper.ObjectRepository import Element
from org.cognizant.mobile.testcase.BaseState import BaseState
#from org.cognizant.mobile.scripthelper.ReusableLibrary import ReusableLibrary
from org.cognizant.mobile.scripthelper.Utility import Utility




class Home(unittest.TestCase):


    def setUp(self):
        
        print('In setup')
        self.lib = Utility()
        
        print(self.id())
        self.report = ResultStore(self.id(), self.mobileBrowser)
        
        # Invoking BaseState
        baseState = BaseState()
        if (self.testData['BaseState'] == 'PublicUS'):
            baseState.PublicUS(self.mobileBrowser)
            pass
        elif (self.testData['BaseState'] == 'PublicUK'):
            baseState.PublicUK(self.mobileBrowser)
            pass        
        pass

    def tearDown(self):
        print('In tearDown')
        self.mobileBrowser.quit()
        TestReport.addToReportList(self.report)               
        pass
    
    def Auto_CopArt_iOS_005(self):
                
        self.report.addReportLog("Step-1", "Step 1 executed", Status.Pass)
        self.report.addReportLog("Step-2", "Step 2 executed", Status.Pass)
        self.report.addReportLog("Step-3", "Step 3 failed", Status.Fail)
        self.assertEqual("A", "B", "Not Same")
        
        pass
    
    def auto_CopArt_iOS_007(self):
        '''
        Registration
        '''
        try:
            # Clicking on Primary Sign In field
            Element('Sign_In_Primary_Android', self.mobileBrowser).click()
        
            # Navigating to Register
            Element('Register_Primary_Android', self.mobileBrowser).click()
        
            # Checking if 
            self.assertTrue(Element('Member_Registration_Android', self.mobileBrowser).isElementPresent(), 'Did not navigate to Registration page')
        
            self.report.addReportLog('Navigating to Registration screen', 'Navigated to Register Screen', Status.Pass)
            
            sEmail = self.lib.fetchDataValue(self.testData, 'Email')
            print('Email: ' + sEmail)
            
            Element('Email_Registration_Android', self.mobileBrowser).setText(sEmail)
            
            Element('Confirm_Email_Registration_Android', self.mobileBrowser).setText(sEmail)
            
            Element('First_Name_Registration_Android', self.mobileBrowser).setText(self.lib.fetchDataValue(self.testData, 'FirstName'))
            Element('Last_Name_Registration_Android', self.mobileBrowser).setText(self.lib.fetchDataValue(self.testData, 'LastName'))
            self.report.addReportLog('Registration of New member', 'Test Fail Status', Status.Fail)
            
        except AssertionError as ase:
            self.report.addReportLog('Registration of new member', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Registration of new member', 'Test Case failed due to unexpected expection: ' + str(exp), Status.Fail)
            
        pass
        
    def auto_CopArt_iOS_008(self):        
        
        try:
            self.lib.signIn(self.mobileBrowser, self.testData)
            
            # Checking if user is signed in
            #Msg_Wait_Android
            Element('Sign_Out_Android', self.mobileBrowser).waitForElement(2)
            self.assertTrue(Element('Sign_Out_Android', self.mobileBrowser).isElementPresent(), 'Could not Sign In successfully')
            self.report.addReportLog('Signing in to application', 'Signed in to application successfully', Status.Pass)
        except AssertionError as ase:
            self.report.addReportLog('Signing in to application', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Signing in to application', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()