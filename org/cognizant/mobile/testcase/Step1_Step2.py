'''
Created on Apr 30, 2015

@author: 209537
'''
import unittest

from org.cognizant.mobile.execution.TestReporting import ResultStore, Status, TestReport
from org.cognizant.mobile.scripthelper.ObjectRepository import Element
from org.cognizant.mobile.testcase.BaseState import BaseState
from org.cognizant.mobile.testcase.Defect import Defect
from datetime import datetime
import time
#from org.cognizant.mobile.scripthelper.ReusableLibrary import ReusableLibrary
from org.cognizant.mobile.scripthelper.Utility import Utility




class Step1_Step2(unittest.TestCase):


    def setUp(self):
        
        print('In setup')
        self.lib = Utility()
        
        print(self.id())
        self.report = ResultStore(self.id(), self.mobileBrowser)
        self.StartTime = datetime.now()
        
        # Invoking BaseState
#         baseState = BaseState()
#         if (self.testData['BaseState'] == 'PublicUS'):
#             baseState.PublicUS(self.mobileBrowser)
#             pass
#         elif (self.testData['BaseState'] == 'PublicUK'):
#             baseState.PublicUK(self.mobileBrowser)
#             pass        
        pass

    def tearDown(self):
        self.mobileBrowser.quit()
        self.EndTime = datetime.now()
        delta = self.EndTime - self.StartTime
        self.report.setExecutionTime(delta)
        TestReport.addToReportList(self.report)                
        pass
    
    #===========================================================================
    # STEP-1 : VIN
    #===========================================================================
    def Copart_ReceivingApp_046(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            if(Element('STEP1_SUBTITLE', self.mobileBrowser).isElementPresent()):
                #REPORT
                if(Element('STEP1_ENTERVIN_TOPROCEED', self.mobileBrowser).isElementPresent()):
                    self.report.addReportLog('VIN Screen has subtitle & proceed message',"Successfully executed", Status.Pass)
                    pass
                    #Report
                else:
                    self.report.addReportLog('STep 1 Missing Message:vinproceed', 'Test Case failed', Status.Fail)
                    pass
                pass
            else:
                self.report.addReportLog('Step 1 Missing Message:subtitle', 'Test Case failed', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('VIN Screen has subtitle & proceed message', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('VIN Screen has subtitle & proceed message', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        pass             
        
    def Copart_ReceivingApp_047(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            dictionary = ['VIN_Toggle_No', 'VIN_Toggle_Yes', 'name:VIN','name:Confirm VIN']
            missing = Defect.dictionary_checkifPresent_helper(self, dictionary, 'Step 1', True)
            if(missing==''):
                Element('VIN_Toggle_No', self.mobileBrowser).bringToVisibility()
                Element('VIN_Toggle_No', self.mobileBrowser).click()
                Element('VIN_Submit', self.mobileBrowser).waitForElement(5)
                Element('VIN_Submit', self.mobileBrowser).click()
                if(Element('name:Vehicle Type', self.mobileBrowser).isElementPresent()):
                    #REPORT SUCCESS
                    self.report.addReportLog('VIN submit',"Successfully executed", Status.Pass)
                    pass
                else:
                    #REPORT UNABLE TO SUBMIT
                    self.report.addReportLog('Unable to submit VIN',"Failed to Execute", Status.Fail)
                    pass
            else:
                # REPORT MISSING
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    def Copart_ReceivingApp_048(self):#ADD TEST DATA AUG 5th
        try:
            Defect.login(self)
            Defect.get_receivinglist_for_submittingWithoutImages(self)
            Defect.select_lot(self,2)
            Element('VIN_Toggle_Yes', self.mobileBrowser).waitForElement(5)
            Element('VIN_Toggle_Yes', self.mobileBrowser).click()
            Defect.navigateToStep_helper(self, '2')
            Defect.step2_basic_info(self)
            Defect.step3_run_condition(self)
            Defect.step4_seller_required_fields(self)
            Defect.step_5_submit_without_images(self)
            Element('Proceed_With_Lot_Submission', self.mobileBrowser).bringToVisibility()
            Element('Proceed_With_Lot_Submission', self.mobileBrowser).click()
            Element('Lot_0',self.mobileBrowser).waitForElement(5)
            if(Element('Lot_0',self.mobileBrowser).isElementPresent()):
                #REPORT SUCCESS
                self.report.addReportLog('Setting VIN YES in Step1 and setting missingVIN as NO IN STEP4.step 5.Proceeded successfully with lot submission',"Successfully executed", Status.Pass)
                pass
            else:
                #REPRT FAIL
                self.report.addReportLog('Unable to submit lot when setting Has Vin in step1 and missing vin no in step4 and proceeding',"Failed to Execute", Status.Fail)
                pass            
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
        pass
    def Copart_ReceivingApp_049(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Element('VIN_Toggle_Yes', self.mobileBrowser).bringToVisibility()
            Element('VIN_Toggle_Yes', self.mobileBrowser).click()
            Element('VIN_Textfield', self.mobileBrowser).bringToVisibility()
            Element('VIN_Textfield', self.mobileBrowser).setTextAndNumForVin(self.lib.fetchDataValue(self.testData, 'correctVIN'))
            vinmismatcherror=str(Defect.Messages.VIN_MISMATCH_ERROR)
            if(Element('name:'+vinmismatcherror,self.mobileBrowser).isElementPresent()):
                #REPORT ERROR
                
                pass
            else:
                Element('VIN_Submit', self.mobileBrowser).bringToVisibility()
                Element('VIN_Submit', self.mobileBrowser).click()
                self.report.addReportLog('Entered correct vin n submitted',"Successfully executed", Status.Pass)
                #REPORT SUCCESS
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    def Copart_ReceivingApp_050(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.step1_vin_screen(self,'')
            Element('name:Vehicle Type',self.mobileBrowser).waitForElement(5)
            if(Element('name:Vehicle Type',self.mobileBrowser).isElementPresent()):
                #REPORT PASS
                self.report.addReportLog('Entered vin n submitted',"Successfully executed", Status.Pass)
                pass
            else:
                #REPORT FAIL
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_051(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            if(Element('id:top_vinval.',self.mobileBrowser).isElementPresent()):
                Element('VIN_Toggle_Yes', self.mobileBrowser).bringToVisibility()
                Element('VIN_Toggle_Yes', self.mobileBrowser).click()
                Element('VIN_Textfield',self.mobileBrowser).bringToVisibility()
                Element('VIN_Textfield', self.mobileBrowser).setTextAndNumForVin(self.lib.fetchDataValue(self.testData, 'VIN'))
                Element('VIN_Mismatch_Alert',self.mobileBrowser).bringToVisibility()
                Element('VIN_Mismatch_Alert',self.mobileBrowser).click()
                Element('ConfirmVIN_Textfield',self.mobileBrowser).bringToVisibility()
                Element('ConfirmVIN_Textfield', self.mobileBrowser).setTextAndNumForVin(self.lib.fetchDataValue(self.testData, 'ConfirmVIN'))
                Element('VIN_CONFIRMVIN_MISMATCH',self.mobileBrowser).waitForElement(3)
                if(Element('VIN_CONFIRMVIN_MISMATCH',self.mobileBrowser).isElementPresent()):
                    #REPORT SUCCESS
                    self.report.addReportLog('Entered different value for Vin and confirm vin n submitted.correct error displayed',"Successfully executed", Status.Pass)
                    pass
                else:
                    #REPORT FAILURE
                    self.report.addReportLog('Checking the Vin mismatch alert failed as the there was no mismatch error thrown',"Failed to Execute", Status.Fail)
                    pass
                pass
            else:
                self.report.addReportLog('Checking the Vin mismatch alert failed as the selected lot does not have a VIN.',"Failed to Execute", Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    #===========================================================================
    # STEP-2: BASIC INFORMATION
    #===========================================================================
    
    def Copart_ReceivingApp_052(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.step1_vin_screen(self,'')
            Element('name:Vehicle Type', self.mobileBrowser).waitForElement(5)
            if(Element('name:Vehicle Type', self.mobileBrowser).isElementPresent()):
                #REPORT PASS SUCCESSFULLY LANDED ON BASIC INFO SCREEN
                self.report.addReportLog('Entered VIN, submitted and landed on step2',"Successfully executed", Status.Pass)
                pass
            else:
                #REPORT FAIL
                self.report.addReportLog('Submitting VIN and landing on step2',"Failed to Execute", Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_053(self):
        try:
            missing=''
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self, i=1)
            Defect.step1_vin_screen(self,'')
            Defect.click_rightdrawer(self)
            Element('Step1', self.mobileBrowser).waitForElement(5)
            if(Element('Step1', self.mobileBrowser).isElementPresent()):
                Defect.click_rightdrawer(self)
                if(Element('STEP2_SUBTITLE', self.mobileBrowser).isElementPresent()):
                    Defect.click_rightdrawer(self)
                    dictionary=['Step1','Step2','Step3','Step4','Step5','Step6']
                    missing=Defect.dictionary_checkifPresent_helper(self, dictionary, 'Right Menu', True)
                    if(missing==''):
                        #Test case successfully executed. Verified step2 subtitle and navigations on right menu
                        self.report.addReportLog('Verified step2 subtitle and navigations on right menu',"Successfully executed", Status.Pass)
                        pass
                    else:
                        #FAIL - Missing options in right menu
                        self.report.addReportLog('Missing option(s) in right menu.Missing:'+missing,"Failed to Execute", Status.Fail)
                        pass
                    pass
                else:
                    #SUBTITLE ON STEP2 MISSING
                    self.report.addReportLog('Missing Sub Title on Step2',"Failed to Execute", Status.Fail)
                    pass
                pass
            else:
                #REPORT FAIL - When the user is on step2, there is no navigation option to go back to step 1 in the right menu
                self.report.addReportLog('When the user is on step2, there is no navigation option to go back to step 1 in the right menu',"Failed to Execute", Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    def Copart_ReceivingApp_056(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self,'2')
            dictionary = ['VehicleType_Finder', 'Year_Step2','Make_Step2','Model_Step2', 'Color_Step2',
                          'name:Has Transmission','name:Has Engine','name:Has Keys', 'name:Odometer','name:Has Plates', 'PrimDamage', 'SecDamage' ]
            missing=Defect.dictionary_checkifPresent_helper(self, dictionary, 'Step2', True)
            if(missing==''):
                Element('HasPlates_Yes', self.mobileBrowser).bringToVisibility()
                Element('HasPlates_Yes', self.mobileBrowser).click()
                dictionary2 = ['name:No. of Plates', 'name:License Plate','LicensePlate', 'Month','Year_LP','State']
                missing2=Defect.dictionary_checkifPresent_helper(self, dictionary2, 'Step2', True)
                if(missing2==''):
                    self.report.addReportLog('Verified all elements on step2',"Successfully executed", Status.Pass)
                    #REPORT PASS
                    pass
                else:
                    #REPORT FAIL
                    self.report.addReportLog('Missing option(s) in step2.Missing:'+missing2,"Failed to Execute", Status.Fail)
                    pass
                pass
            else:
                #REPORT FAIL
                self.report.addReportLog('Missing option(s) in step2.Missing:'+missing,"Failed to Execute", Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    def Copart_ReceivingApp_057(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '2')
            dictionary = ['VehicleType_Finder', 'Year_Step2','Make_Step2','Model_Step2', 'Color_Step2']
            for x in dictionary:
                Element(x,self.mobileBrowser).bringToVisibility()
                Element(x,self.mobileBrowser).click()
                time.sleep(2)
#                 Element('BackArrow',self.mobileBrowser).waitForElement(10)
                Element('BackArrow',self.mobileBrowser).click()
                pass
            Step1_Step2.take_engine_picture(self)
            dictn = ['HasTrans_Yes', 'HasTrans_No', 'HasEngine_Yes','HasEngine_No','HasKeys_Yes','HasKeys_No','HasKeys_Exempt', 'Odometer_Reading','Odometer_Digital', 'Odometer_Damaged','HasPlates_Yes','HasPlates_No','PrimDamage', 'SecDamage']
            missing = Defect.dictionary_checkifPresent_helper(self, dictn, 'Step2', True)
            if(missing==''):
                Step1_Step2.take_odometer_picture(self)
                Element('Odometer_Digital', self.mobileBrowser).bringToVisibility()
                Element('Odometer_Digital', self.mobileBrowser).click()
                if(Element('Odometer_Reading', self.mobileBrowser).isElementPresent()):
                    #REPORT FAIL
                    pass
                else:
                    Element('HasPlates_Yes', self.mobileBrowser).bringToVisibility()
                    Element('HasPlates_Yes', self.mobileBrowser).click()
                    dict2 = ['Plates_One', 'Plates_Two','LicensePlate','Month','Year_LP','State']
                    missing2 = Defect.dictionary_checkifPresent_helper(self, dict2, 'Step2', True)
                    if(missing2==''):
                        Defect.step2_basic_info(self,2, False)
                        #REPORT SUCCESS
                        self.report.addReportLog('Verified behavior of dropdown and elements on screen. took engine and odom pics',"Successfully executed", Status.Pass)
                        pass
                    pass
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_059(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '2')
            Element('Odometer_Reading', self.mobileBrowser).bringToVisibility()
            Element('Odometer_Reading', self.mobileBrowser).setNumeric('123')
            if(Element('Odometer_Digital', self.mobileBrowser).isenabled() or Element('Odometer_Damaged', self.mobileBrowser).isenabled()):
                #REPORT FAIL
                pass
            else:
                Step1_Step2.take_odometer_picture(self)
                if(Element('Odometer_Reading', self.mobileBrowser).isElementPresent()):
                    #REPORT SUCCESS
                    self.report.addReportLog('Entered Odometer and verified that digital n damaged buttons disabled',"Successfully executed", Status.Pass)
                    pass
                else:
                    #FAIL
                    pass
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_060(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '2')
            Element('Odometer_Reading', self.mobileBrowser).bringToVisibility()
            if(Element('name:Enter odometer reading', self.mobileBrowser).isElementPresent()):
                Element('Odometer_Reading', self.mobileBrowser).setNumeric('12345678')
                if(Element('name:12345678', self.mobileBrowser).isElementPresent()):
                    #REPORT FAIL
                    self.report.addReportLog('Verifying the default odometer message . disappearing after typing odom value',"Failed to Execute", Status.Fail)
                    
                    pass
                else:
                    #REPORT PASS
                    self.report.addReportLog('Verified the default odometer message and it disappeared after typing odom value',"Successfully executed", Status.Pass)
                    pass
                pass
            else:
                #DEFAULT ODOM TEXT MSSING
                self.report.addReportLog('Default Odometer text missing', 'Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('EXCEPTION', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('EXCEPTION', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_061(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '2')
            Element('HasPlates_Yes',self.mobileBrowser).bringToVisibility()
            Element('HasPlates_Yes',self.mobileBrowser).click()
                
            tempv = 'NoOfPlates_Lot'
            tempv=self.lib.fetchDataValue(self.testData, tempv)
            Element(tempv,self.mobileBrowser).bringToVisibility()
            Element(tempv,self.mobileBrowser).click()       
            temp = 'LicensePlate_Lot'
            temp = self.lib.fetchDataValue(self.testData, temp)
            #MONTH
            temp = 'Month_Lot'
            temp = self.lib.fetchDataValue(self.testData, temp)
            Element('Month',self.mobileBrowser).bringToVisibility()
            Element('Month',self.mobileBrowser).click()
            Element('name:'+temp, self.mobileBrowser).bringToVisibility()
            Element('name:'+temp, self.mobileBrowser).click()
            #YEAR
            temp = 'LP_Year_Lot'
            temp = self.lib.fetchDataValue(self.testData, temp)
            Element('Year_LP',self.mobileBrowser).bringToVisibility()
            Element('Year_LP',self.mobileBrowser).click()
            Element('name:'+temp, self.mobileBrowser).bringToVisibility()
            Element('name:'+temp, self.mobileBrowser).click()
            #STATE
            temp = 'State_Lot'
            temp = self.lib.fetchDataValue(self.testData, temp)
            Element('State',self.mobileBrowser).bringToVisibility()
            Element('State',self.mobileBrowser).click()
            Element('name:'+temp, self.mobileBrowser).bringToVisibility()
            Element('name:'+temp, self.mobileBrowser).click()
            self.report.addReportLog('Verified Plates behavior',"Successfully executed", Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('Step2 Basic Information Screen Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Step2 Basic Information Screen Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass
    def Copart_ReceivingApp_062(self):
        try:
            i=''
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '2')
            if(Element('name:Please Select',self.mobileBrowser).isElementPresent()):
                temp ='VehicleType_Lot'+i
                temp=self.lib.fetchDataValue(self.testData, temp)
                Element('VehicleType_Finder', self.mobileBrowser).bringToVisibility()
                Element('VehicleType_Finder', self.mobileBrowser).click() 
                 
                Element('name:' + temp, self.mobileBrowser).bringToVisibility()
                Element('name:' + temp, self.mobileBrowser).click()
                #YEAR
                temp='Year_Lot'+i
                temp=self.lib.fetchDataValue(self.testData, temp)
                Element('Year_Step2', self.mobileBrowser).click()  
                Element('name:' + temp, self.mobileBrowser).bringToVisibility()
                Element('name:' + temp, self.mobileBrowser).click()
                #MAKE
                temp = 'Make_Lot'+i
                temp=self.lib.fetchDataValue(self.testData, temp)
                Element('Make_Step2', self.mobileBrowser).click()  
                Element('name:' + temp, self.mobileBrowser).bringToVisibility()
                Element('name:' + temp, self.mobileBrowser).click()
                #MODEL
                temp = 'Model_Lot'+i
                temp=self.lib.fetchDataValue(self.testData, temp)
                Element('Model_Step2', self.mobileBrowser).click()  
                Element('name:' + temp, self.mobileBrowser).bringToVisibility()
                Element('name:' + temp, self.mobileBrowser).click()
                
                #HAS TRANSMISSION
                temp = 'HasTransmission_Lot'+i
                temp=self.lib.fetchDataValue(self.testData, temp)
                Element(temp, self.mobileBrowser).bringToVisibility()
                Element(temp, self.mobileBrowser).click()
                #HAS ENGINE
                temp = 'HasEngine_Lot'+i
                temp=self.lib.fetchDataValue(self.testData, temp)
                Element(temp,self.mobileBrowser).bringToVisibility()
                Element(temp,self.mobileBrowser).click()
                
                #HAS KEYS
                temp='HasKeys_Lot'+i
                temp=self.lib.fetchDataValue(self.testData, temp)
                Element(temp,self.mobileBrowser).bringToVisibility()
                Element(temp,self.mobileBrowser).click()
                #ODOMETER
                determineOdom = 'Odometer_Lot'+i
                determineOdomValue = self.lib.fetchDataValue(self.testData, determineOdom)
                if(determineOdomValue != 'NONE'):
                    Element('Odometer_Reading', self.mobileBrowser).bringToVisibility()
                    Element('Odometer_Reading', self.mobileBrowser).setNumeric(determineOdomValue)
                    pass
                else:                 
                    tempOdom = 'OdometerButton_Lot' + i
                    tempOdom = self.lib.fetchDataValue(self.testData, tempOdom)
                    Element(tempOdom, self.mobileBrowser).bringToVisibility()
                    Element(tempOdom, self.mobileBrowser).click()
                    pass
                
                #HAS PLATES
                temp = 'HasPlates_Lot'+i
                temp=self.lib.fetchDataValue(self.testData, temp)
                Element(temp,self.mobileBrowser).bringToVisibility()
                Element(temp,self.mobileBrowser).click()
                 
                #NO. OF PLATES
                if(temp=='HasPlates_Yes'):
                    tempv = 'NoOfPlates_Lot'+i
                    tempv=self.lib.fetchDataValue(self.testData, tempv)
                    Element(tempv,self.mobileBrowser).bringToVisibility()
                    Element(tempv,self.mobileBrowser).click()       
                #LICENSE PLATE
                    temp = 'LicensePlate_Lot'+i
                    temp = self.lib.fetchDataValue(self.testData, temp)
                #MONTH
                    temp = 'Month_Lot'+i
                    temp = self.lib.fetchDataValue(self.testData, temp)
                    Element('Month',self.mobileBrowser).bringToVisibility()
                    Element('Month',self.mobileBrowser).click()
                    Element('name:'+temp, self.mobileBrowser).bringToVisibility()
                    Element('name:'+temp, self.mobileBrowser).click()
                #YEAR
                    temp = 'LP_Year_Lot'+i
                    temp = self.lib.fetchDataValue(self.testData, temp)
                    Element('Year_LP',self.mobileBrowser).bringToVisibility()
                    Element('Year_LP',self.mobileBrowser).click()
                    Element('name:'+temp, self.mobileBrowser).bringToVisibility()
                    Element('name:'+temp, self.mobileBrowser).click()
                #STATE
                    temp = 'State_Lot'+i
                    temp = self.lib.fetchDataValue(self.testData, temp)
                    Element('State',self.mobileBrowser).bringToVisibility()
                    Element('State',self.mobileBrowser).click()
                    Element('name:'+temp, self.mobileBrowser).bringToVisibility()
                    Element('name:'+temp, self.mobileBrowser).click()
                    pass
            
                else:
                    pass
                 
                #PRIMARY DAMAGE
                temp = 'PrimaryDamage_Lot'+i
                temp=self.lib.fetchDataValue(self.testData, temp)
                Element('PrimDamage', self.mobileBrowser).bringToVisibility()
                Element('PrimDamage', self.mobileBrowser).click()  
                Element('name:' + temp, self.mobileBrowser).bringToVisibility()
                Element('name:' + temp, self.mobileBrowser).click()
                #SECONDARY DAMAGE
                temp = 'SecondaryDamage_Lot'+i
                temp=self.lib.fetchDataValue(self.testData, temp)
                Element('SecDamage', self.mobileBrowser).bringToVisibility()
                Element('SecDamage', self.mobileBrowser).click()  
                Element('name:' + temp, self.mobileBrowser).bringToVisibility()
                Element('name:' + temp, self.mobileBrowser).click()
                Element('SaveAndContinue', self.mobileBrowser).bringToVisibility() 
                Element('SaveAndContinue', self.mobileBrowser).click()
                if(Element('name:Color value cannot be Unknown',self.mobileBrowser).isElementPresent()):
                    #REPORT TEST CASE PASSED
                    self.report.addReportLog('submitted infor without color value. verified the color error msg',"Successfully executed", Status.Pass)
                    pass
                else:
                    #FIND A LOT WITH MISSING COLOR VALUE
                    pass
                pass
            else:
                #REPORT COLOR IS ALREADY SELECTED
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('Step2 Basic Information Screen Failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('Step2 Basic Information Screen Failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
        pass

    
    def take_odometer_picture(self, fickup=False):
        Element('id:odometer_image.', self.mobileBrowser).bringToVisibility(fickup)
        Element('id:odometer_image.', self.mobileBrowser).click()
        time.sleep(3)
        Element.presskey(self,keycode=24)
        time.sleep(2)
        Element('name:OK', self.mobileBrowser).bringToVisibility(False,True)
        if(Element('name:OK', self.mobileBrowser).isElementPresent()):
            Element('name:OK', self.mobileBrowser).click()
            pass
        else:
            Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).bringToVisibility()
            Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).click()
            pass
        time.sleep(1)
        pass
    def take_engine_picture(self,fickup=False):
        Element('id:engine_image.', self.mobileBrowser).bringToVisibility(fickup)
        Element('id:engine_image.', self.mobileBrowser).click()
        time.sleep(3)
        Element.presskey(self,keycode=24)
        time.sleep(2)
        Element('name:OK', self.mobileBrowser).bringToVisibility(False,True)
        if(Element('name:OK', self.mobileBrowser).isElementPresent()):
            Element('name:OK', self.mobileBrowser).click()
            pass
        else:
            Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).bringToVisibility()
            Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).click()
            pass
        time.sleep(1)
        pass
        
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()