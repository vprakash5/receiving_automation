'''
Created on May 20, 2015

@author: 209537
'''
from org.cognizant.mobile.scripthelper.ObjectRepository import Element
from org.cognizant.mobile.scripthelper.ReusableLibrary import ReusableLibrary

class BaseState(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        pass
    
    def PublicUS(self, mobileDriver):
        # Clicking on Settings Button on launch
        Element('Login_Left_Menu_Btn', mobileDriver).waitForElement(20)
        if(Element('Login_Left_Menu_Btn', mobileDriver).isElementPresent()):
            Element('Login_Left_Menu_Btn', mobileDriver).click()            
#             # Clicking on Vehicle Source
            Element('name:Settings', mobileDriver).bringToVisibility()
            Element('name:Settings', mobileDriver).click()
            
            Element('Settings_VehicleSourceLanguage',mobileDriver).bringToVisibility()
            Element('Settings_VehicleSourceLanguage', mobileDriver).click()
            
            Element('Settings_VehicleSource',mobileDriver).bringToVisibility()
            Element('Settings_VehicleSource', mobileDriver).click()
            
            Element('name:North America',mobileDriver).bringToVisibility()
            Element('name:North America', mobileDriver).click()
            
            print('Set US App Successfully')
#                     
#             # Selecting North America as Vehicle source
#             Element('RdoBtn_VS_NA_Android', mobileDriver).click()
#             
#             Element('Page_Up_Android', mobileDriver).waitForElement(3) 
#             Element('Page_Up_Android', mobileDriver).click()
#             pass
        else:
            pass
     
    
    def PublicUK(self, mobileDriver):
         # Clicking on Settings Button on launch
        Element('Login_Left_Menu_Btn', mobileDriver).waitForElement()
        if(Element('Login_Left_Menu_Btn', mobileDriver).isElementPresent()):
            Element('Login_Left_Menu_Btn', mobileDriver).click()            
#             # Clicking on Vehicle Source
            Element('name:Settings', mobileDriver).bringToVisibility()
            Element('name:Settings', mobileDriver).click()
            
            Element('Settings_VehicleSourceLanguage',mobileDriver).bringToVisibility()
            Element('Settings_VehicleSourceLanguage', mobileDriver).click()
            
            Element('Settings_VehicleSource',mobileDriver).bringToVisibility()
            Element('Settings_VehicleSource', mobileDriver).click()
            
            Element('name:UK',mobileDriver).bringToVisibility()
            Element('name:UK', mobileDriver).click()
            
            print('Set US App Successfully')
#                     
#             # Selecting North America as Vehicle source
#             Element('RdoBtn_VS_NA_Android', mobileDriver).click()
#             
#             Element('Page_Up_Android', mobileDriver).waitForElement(3) 
#             Element('Page_Up_Android', mobileDriver).click()
#             pass
        else:
            pass