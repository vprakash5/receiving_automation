'''
Created on Apr 30, 2015

@author: 209537
'''
import unittest
import time
from org.cognizant.mobile.execution.TestReporting import ResultStore, Status, TestReport
from org.cognizant.mobile.scripthelper.ObjectRepository import Element
from org.cognizant.mobile.testcase.BaseState import BaseState
from org.cognizant.mobile.testcase.Defect import Defect
from time import sleep
from datetime import datetime

#from org.cognizant.mobile.scripthelper.ReusableLibrary import ReusableLibrary
from org.cognizant.mobile.scripthelper.Utility import Utility




class Step5_Step6(unittest.TestCase):


    def setUp(self):
        
        print('In setup')
        self.lib = Utility()
        
        print(self.id())
        self.report = ResultStore(self.id(), self.mobileBrowser)
        self.StartTime = datetime.now()
        
        # Invoking BaseState
#         baseState = BaseState()
#         if (self.testData['BaseState'] == 'PublicUS'):
#             baseState.PublicUS(self.mobileBrowser)
#             pass
#         elif (self.testData['BaseState'] == 'PublicUK'):
#             baseState.PublicUK(self.mobileBrowser)
#             pass        
        pass

    def tearDown(self):
        self.mobileBrowser.quit()
        self.EndTime = datetime.now()
        delta = self.EndTime - self.StartTime
        self.report.setExecutionTime(delta)
        TestReport.addToReportList(self.report)                 
        pass
    
    #===========================================================================
    # STEP-5: SERVICE ORDERS
    #===========================================================================
    def Copart_ReceivingApp_074(self):
        missing=''
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '4')
            Defect.step4_seller_required_fields(self)
            dictionary = ['id:top_vinlabel.','id:top_lotlabel.','id:top_losstypelabel.','STEP5_SUBTITLE','id:ServiceOrder_Continue.','SuggestOrder_SubmitNoImages']
            missing = Defect.dictionary_checkifPresent_helper(self, dictionary, 'Step5', True)
            print('missing:'+missing)
            if(missing==''):
                #Go FURTHER
                self.report.addReportLog('Locating elements on step5', 'Successfully Executed', Status.Pass)
                
                pass
            else:
                #FAILED TO LOCATE ELEMNTS
                self.report.addReportLog('Locating elements on step5. Missing:'+missing, 'Failed to execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('074:'+missing, 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    def Copart_ReceivingApp_075(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '5')
            Element('SuggestOrder_Select', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_Select', self.mobileBrowser).click()
            dictionary = ['id:SuggestOrder_No_0.','id:SuggestOrder_No_1.','id:SuggestOrder_No_2.','id:SuggestOrder_No_3.','id:SuggestOrder_No_4.','id:SuggestOrder_No_5.','id:SuggestOrder_No_6.','id:SuggestOrder_No_7.','id:SuggestOrder_No_8.','id:SuggestOrder_No_9.']
            missing=Defect.dictionary_checkifPresent_helper(self, dictionary, 'Suggest Service Order', True)
            print('missing:'+missing)
            if(missing==''):
                #Go FURTHER
                self.report.addReportLog('Locating elements on step5 Suggest WO list', 'Successfully Executed', Status.Pass)
                
                pass
            else:
                #FAILED TO LOCATE ELEMNTS
                self.report.addReportLog('Locating elements on step5 Suggest WO list. Missing:'+missing, 'Failed to execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_076(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '4')
            Defect.step4_seller_required_fields(self)
            Element('SuggestOrder_Select', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_Select', self.mobileBrowser).click()
            Element('BackArrow', self.mobileBrowser).bringToVisibility()
            Element('BackArrow', self.mobileBrowser).click()
            Element('SuggestOrder_Select', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_Select', self.mobileBrowser).click()
            Element('id:SuggestOrder_No_0.', self.mobileBrowser).bringToVisibility()
            Element('id:SuggestOrder_No_0.', self.mobileBrowser).click()
            Element('BackArrow', self.mobileBrowser).bringToVisibility()
            Element('BackArrow', self.mobileBrowser).click()
            Element('SuggestOrder_Select', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_Select', self.mobileBrowser).click()
            if(Element('name:YES',self.mobileBrowser).isElementPresent()):
                #REPORT FAILURE
                self.report.addReportLog('Locating changes on  Suggest WO list', 'Failed to execute', Status.Fail)
                pass
            else:
                #REPORT PASS
                self.report.addReportLog('Locating changes on  Suggest WO list', 'Successfully Executed', Status.Pass)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    def Copart_ReceivingApp_077(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '4')
            Defect.step4_seller_required_fields(self)
            Defect.step5_service_orders(self)
            Element('STEP6_SUBTITLE',self.mobileBrowser).bringToVisibility()
            if(Element('STEP6_SUBTITLE',self.mobileBrowser).isElementPresent()):
                #REPORT PASS
                self.report.addReportLog('Successfully finished step 5.Landed on Step 6', 'Successfully Executed', Status.Pass)
                pass
            else:
                #REPORT FAILURE
                self.report.addReportLog('Finishing step 5. Landing on step6', 'Failed to execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    
        
    def Copart_ReceivingApp_078(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist_for_submittingWithoutImages(self)
            Defect.select_lot(self)
            Defect.step1_vin_screen(self)
            Defect.step2_basic_info(self)
            Defect.step3_run_condition(self)
            Defect.step4_seller_required_fields(self)
            Element('SuggestOrder_Select', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_Select', self.mobileBrowser).click()
            Element('id:SuggestOrder_No_0.', self.mobileBrowser).bringToVisibility()
            Element('id:SuggestOrder_No_0.', self.mobileBrowser).click()
            Element('id:SuggestOrder_No_1.', self.mobileBrowser).bringToVisibility()
            Element('id:SuggestOrder_No_1.', self.mobileBrowser).click()
            Element('id:SuggestOrder_No_2.', self.mobileBrowser).bringToVisibility()
            Element('id:SuggestOrder_No_2.', self.mobileBrowser).click()
            Element('SuggestOrder_SaveNGo', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_SaveNGo', self.mobileBrowser).click()
            Element('SuggestOrder_SubmitNoImages', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_SubmitNoImages', self.mobileBrowser).click()
            if(Element('PROCEED_WITH_LOT_SUBMISSION_MSG',self.mobileBrowser).isElementPresent()):#1FIRST IF STARTS HERE
                Element('Proceed_With_Lot_Submission', self.mobileBrowser).bringToVisibility()
                Element('Proceed_With_Lot_Submission', self.mobileBrowser).click()
                if(Element('Filter_Button', self.mobileBrowser).isElementPresent()):#2SECOND IF
                    Defect.click_leftdrawer(self)
                    Element('LeftMenu_HomeScreen', self.mobileBrowser).bringToVisibility()
                    Element('LeftMenu_HomeScreen', self.mobileBrowser).click()
                    Defect.get_imagedecouplinglist(self)
                    Defect.select_imaging_lot(self)
                    Element('id:SaveAndComplete.', self.mobileBrowser).bringToVisibility()
                    Element('id:SaveAndComplete.', self.mobileBrowser).click()
                    Element('Image_Decoup_ReqImages_NotTaken',self.mobileBrowser).bringToVisibility()
                    if(Element('Image_Decoup_ReqImages_NotTaken',self.mobileBrowser).isElementPresent()):#3RD IF
                        Element('name:OK', self.mobileBrowser).bringToVisibility()
                        Element('name:OK', self.mobileBrowser).click()
                        images = ['left_1.', 'right_1.', 'left_2.','right_2.', 'left_3.', 'right_3.', 'left_4.', 'right_4.','left_5.', 'right_5.']
                        Element('id:left_1.', self.mobileBrowser).bringToVisibility(True)
                        for x in images:
                            Element('id:'+x, self.mobileBrowser).bringToVisibility()
                            Element('id:'+x, self.mobileBrowser).click()
                            sleep(3)
                            Element.presskey(self,keycode=24)
                            sleep(2)
                            Element('name:OK', self.mobileBrowser).bringToVisibility(False,True)
                            if(Element('name:OK', self.mobileBrowser).isElementPresent()):#4TH IF
                                Element('name:OK', self.mobileBrowser).click()
                                pass
                            else:#4th ELSE
                                Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).bringToVisibility()
                                Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).click()
                                pass
                            pass#FOR CLOSING
                        Element('id:SaveAndComplete.', self.mobileBrowser).bringToVisibility()
                        Element('id:SaveAndComplete.', self.mobileBrowser).click()
                        Element('name:Cancel', self.mobileBrowser).bringToVisibility()
                        Element('name:Cancel', self.mobileBrowser).click()
                        Element('id:SaveAndComplete.', self.mobileBrowser).bringToVisibility()
                        Element('id:SaveAndComplete.', self.mobileBrowser).click()
                        Element('id:android:id/button1', self.mobileBrowser).bringToVisibility()
                        Element('id:android:id/button1', self.mobileBrowser).click()
                        self.report.addReportLog('', 'Successfully Executed', Status.Pass)
                        pass
                    else:#3RD ELSE
                        #REPORT IMAGE DECOUP REQ IMAGES NOT TAKEN MSG MISSING
                        self.report.addReportLog('IMAGE DECOUP REQ IMAGES NOT TAKEN MSG MISSING','Failed to Execute', Status.Fail)
                        pass
                    pass
                else:#2nd Else
                    #REPORT FILTER BUTTON MISSING
                    self.report.addReportLog('FILTER BUTTON MISSING','Failed to Execute', Status.Fail)
                    pass
                pass
            else:#FIRST ELSE
                #REOPRT PROCEED WITH LOT SUBMISSION MSG MISSING
                self.report.addReportLog('PROCEED WITH LOT SUBMISSION MSG MISSING','Failed to Execute', Status.Fail)
                pass
            pass #TRY ENDS
        except AssertionError as ase:
            self.report.addReportLog('TEST', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TEST', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass   
        
    def Copart_ReceivingApp_079(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '4')
            Defect.step4_seller_required_fields(self)
            Element('id:ServiceOrder_Continue.', self.mobileBrowser).bringToVisibility()
            Element('id:ServiceOrder_Continue.', self.mobileBrowser).click()
            Element('STEP6_SUBTITLE',self.mobileBrowser).bringToVisibility()
            if(Element('STEP6_SUBTITLE',self.mobileBrowser).isElementPresent()):
                #REPORT SUCCESS
                self.report.addReportLog('Test', 'Successfully Executed', Status.Pass)
                pass
            else:
                #REPORT FAILURE
                self.report.addReportLog('Test', 'Failed to execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    def Copart_ReceivingApp_080(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '4')
            Defect.step4_seller_required_fields(self)
            Element('SuggestOrder_Select', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_Select', self.mobileBrowser).click()
            Element('id:SuggestOrder_No_0.', self.mobileBrowser).bringToVisibility()
            Element('id:SuggestOrder_No_0.', self.mobileBrowser).click()
            Element('id:SuggestOrder_No_1.', self.mobileBrowser).bringToVisibility()
            Element('id:SuggestOrder_No_1.', self.mobileBrowser).click()
            Element('id:SuggestOrder_No_2.', self.mobileBrowser).bringToVisibility()
            Element('id:SuggestOrder_No_2.', self.mobileBrowser).click()
            Element('SuggestOrder_SaveNGo', self.mobileBrowser).bringToVisibility()
            Element('SuggestOrder_SaveNGo', self.mobileBrowser).click()
            Defect.navigateToStep_helper(self, '4')
            if(Element('STEP4_SUBTITLE', self.mobileBrowser).isElementPresent()):
                #Report Success
                self.report.addReportLog('Test', 'Successfully Executed', Status.Pass)
                pass
            else:
                #Report failure
                self.report.addReportLog('Test', 'Failed to execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
     
    
    #===========================================================================
    # STEP-6: PICTURES
    #===========================================================================
    
    def Copart_ReceivingApp_081(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '5')
            Defect.step5_service_orders(self)
            images = ['id:left_1.', 'id:right_1.', 'id:left_2.','id:right_2.', 'id:left_3.', 'id:right_3.', 'id:left_4.', 'id:right_4.','id:left_5.', 'id:right_5.','STEP6_SUBTITLE','PICTURES_SCREEN_HELPER_MSG']
            missing= Defect.dictionary_checkifPresent_helper(self, images, 'Pictures', True)
            imagenames=['name:Passenger side front angle','name:Driver side front angle','name:Driver side rear angle','name:Passenger side rear angle','name:Passenger front interior','name:Passenger rear interior','name:Open hood engine front','name:Odometer','name:Damage/Feature #1','name:Damage/Feature #2']
            Element('name:Passenger side front angle', self.mobileBrowser).bringToVisibility(True)
            missingimageNames= Defect.dictionary_checkifPresent_helper(self, imagenames, 'Pictures', True)
            if(missing=='' and missingimageNames==''):
                #REPORT SUCCESS
                self.report.addReportLog('Test', 'Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Verifying elements on pictures screen but missing:'+missing+' '+missingimageNames, 'Pictures', Status.Pass)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    
    def Copart_ReceivingApp_082(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '5')
            Defect.step5_service_orders(self)
            Element('id:left_1.', self.mobileBrowser).bringToVisibility()
            Element('id:left_1.', self.mobileBrowser).click()
            Element.presskey(self, keycode=4)
            if(Element('id:left_1.', self.mobileBrowser).isElementPresent()):
                Element('id:left_1.', self.mobileBrowser).bringToVisibility()
                Element('id:left_1.', self.mobileBrowser).click()
                time.sleep(3)
                Element.presskey(self,keycode=24)
                time.sleep(2)
                
                imagekeys = [str(Defect.Pictures.APPROVE_BUTTON_ID), str(Defect.Pictures.CANCEL_BUTTON_ID)]
                missing=''
                missing=Defect.dictionary_checkifPresent_helper(self, imagekeys, 'Pictures Camera', True)
                if(missing==''):
                    Element('id:com.motorola.camera:id/review_cancel',self.mobileBrowser).click()
                    time.sleep(3)
                    Element.presskey(self,keycode=24)
                    time.sleep(2)
                    Element('name:OK', self.mobileBrowser).bringToVisibility(False,True)
                    if(Element('name:OK', self.mobileBrowser).isElementPresent()):
                        Element('name:OK', self.mobileBrowser).click()
                        pass
                    else:
                        Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).bringToVisibility()
                        Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).click()
                        pass
                    if(Element('id:left_2.', self.mobileBrowser).isElementPresent()):
                        #REPORT PASS
                        self.report.addReportLog('Test', 'Successfully Executed', Status.Pass)
                        pass
                    else:
                        #REPORT FAIL
                        self.report.addReportLog('Test', 'Failed to execute', Status.Fail)
                        pass
                    pass
                else:
                    #REPORT FAIL
                    self.report.addReportLog('Test', 'Failed to execute', Status.Fail)
                    pass
                pass
            else:
                #REPORT FAIL
                self.report.addReportLog('Test', 'Failed to execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    def Copart_ReceivingApp_083(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self,'4')
            Defect.step4_seller_required_fields(self)
            Defect.step5_service_orders(self)
            Defect.step6_images(self, False)
            Element('id:left_5.', self.mobileBrowser).waitForElement(6)
            Element('id:left_5.', self.mobileBrowser).click()
            Element('Retake_Images_Msg',self.mobileBrowser).waitForElement(5)
            if(Element('Retake_Images_Msg',self.mobileBrowser).isElementPresent()):
                Element('name:No',self.mobileBrowser).click()
                if(Element('id:left_5.', self.mobileBrowser).isElementPresent()):
                    self.report.addReportLog('verified re-take image message.clicked No and no changes occurred to images as expected', 'Successfully Executed', Status.Pass)
                    pass
                pass
            else:
                self.report.addReportLog('Retake image message missing when user clicked on clicked image thumbnail', 'Failed to execute', Status.Fail)
                pass
            
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    def Copart_ReceivingApp_084(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.step1_vin_screen(self)
            Defect.step2_basic_info(self)
            Defect.step3_run_condition(self)
            Defect.step4_seller_required_fields(self)
            Defect.step5_service_orders(self)
            images = ['left_1.', 'right_1.', 'left_2.','right_2.', 'left_3.', 'right_3.', 'left_4.', 'right_4.']
            for x in images:
                Element('id:'+x, self.mobileBrowser).bringToVisibility()
                Element('id:'+x, self.mobileBrowser).click()
                time.sleep(3)
                Element.presskey(self,keycode=24)
                time.sleep(2)
                Element('name:OK', self.mobileBrowser).bringToVisibility(False,True)
                if(Element('name:OK', self.mobileBrowser).isElementPresent()):
                    Element('name:OK', self.mobileBrowser).click()
                    pass
                else:
                    Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).bringToVisibility()
                    Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).click()
                    pass
                time.sleep(1)
                pass
            Element('id:SaveAndComplete.', self.mobileBrowser).bringToVisibility()
            Element('id:SaveAndComplete.', self.mobileBrowser).click()
            if(Element('CAPTURE_ALL_IMAGES_ERROR', self.mobileBrowser).isElementPresent()):
                lasttwo = ['left_5.', 'right_5.']
                for k in lasttwo:
                    Element('id:'+k, self.mobileBrowser).bringToVisibility()
                    Element('id:'+k, self.mobileBrowser).click()
                    time.sleep(3)
                    Element.presskey(self,keycode=24)
                    time.sleep(2)
                    Element('name:OK', self.mobileBrowser).bringToVisibility(False,True)
                if(Element('name:OK', self.mobileBrowser).isElementPresent()):
                    Element('name:OK', self.mobileBrowser).click()
                    pass
                else:
                    Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).bringToVisibility()
                    Element('id:com.motorola.camera:id/review_approve', self.mobileBrowser).click()
                    pass
                    time.sleep(1)
                    pass
                Element('id:SaveAndComplete.', self.mobileBrowser).bringToVisibility()
                Element('id:SaveAndComplete.', self.mobileBrowser).click()
                if(Element('Search_TextField', self.mobileBrowser).isElementPresent()):
                    #REPORT PASS
                    self.report.addReportLog('Test', 'Successfully Executed', Status.Pass)
                    pass
                else:
                    #REPORT FAIL
                    self.report.addReportLog('Test', 'Failed to execute', Status.Fail)
                    pass
                pass
            else:
                #REPORT FAIL
                self.report.addReportLog('Test', 'Failed to execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_086(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '4')
            Defect.step4_seller_required_fields(self)
            if(Element('SuggestOrder_Select', self.mobileBrowser).isElementPresent()):
                Defect.click_leftdrawer(self)
                Element('LeftMenu_HomeScreen', self.mobileBrowser).bringToVisibility()
                Element('LeftMenu_HomeScreen', self.mobileBrowser).click()
                if(Element('Receiving_List', self.mobileBrowser).isElementPresent()):
                    #REPORT PASS
                    self.report.addReportLog('Test', 'Successfully Executed', Status.Pass)
                    pass
                else:
                    #REPORT FAIL
                    self.report.addReportLog('Test', 'Failed to execute', Status.Fail)
                    pass
                pass
            else:
                #FAIL
                self.report.addReportLog('Test', 'Failed to execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    def x(self):
        print('1st TC')
        pass
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()