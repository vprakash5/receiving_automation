'''
Created on Apr 30, 2015

@author: 209537
'''
import unittest

from org.cognizant.mobile.execution.TestReporting import ResultStore, Status, TestReport
from org.cognizant.mobile.scripthelper.ObjectRepository import Element
from org.cognizant.mobile.testcase.BaseState import BaseState
from org.cognizant.mobile.testcase.Defect import Defect
from time import sleep
from datetime import datetime
#from org.cognizant.mobile.scripthelper.ReusableLibrary import ReusableLibrary
from org.cognizant.mobile.scripthelper.Utility import Utility




class Step3_Step4(unittest.TestCase):


    def setUp(self):
        
        print('In setup')
        self.lib = Utility()
        
        print(self.id())
        self.report = ResultStore(self.id(), self.mobileBrowser)
        self.StartTime = datetime.now()
        
        # Invoking BaseState
#         baseState = BaseState()
#         if (self.testData['BaseState'] == 'PublicUS'):
#             baseState.PublicUS(self.mobileBrowser)
#             pass
#         elif (self.testData['BaseState'] == 'PublicUK'):
#             baseState.PublicUK(self.mobileBrowser)
#             pass        
        pass

    def tearDown(self):
        self.mobileBrowser.quit()
        self.EndTime = datetime.now()
        delta = self.EndTime - self.StartTime
        self.report.setExecutionTime(delta)
        TestReport.addToReportList(self.report)                 
        pass
    
    #===========================================================================
    # STEP-3: RUN CONDITION
    #===========================================================================
    
    def Copart_ReceivingApp_064(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self,'3')
            if(Element('STEP3_SUBTITLE', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Found Elements on step 3', 'Successfully Executed', Status.Pass)
                pass
            else:
                self.report.addReportLog('Finding subtitle', 'Failed due to missing element(s)', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_065(self):
        try:
            missing=''
            dictionary = ['RunCondition_Drives','RunCondition_Starts','RunCondition_None']
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self,'3')
            missing=Defect.dictionary_checkifPresent_helper(self, dictionary, 'Step 3', True)
            if(missing!=''):
                self.report.addReportLog('Finding Elements on step 3 failed. Missing:'+missing, 'Failed due to missing element(s)', Status.Fail)
                pass
            else:
                self.report.addReportLog('Found Elements on step 3', 'Successfully Executed', Status.Pass)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_066(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self,'3')
            temp = 'RunCondition_Lot'
            temp = self.lib.fetchDataValue(self.testData, temp)
            Element(temp, self.mobileBrowser).waitForElement(7)
            Element(temp, self.mobileBrowser).click()
            Element('RunCondition_Save', self.mobileBrowser).bringToVisibility()
            Element('RunCondition_Save', self.mobileBrowser).click()
            if(Element('STEP4_SUBTITLE' , self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Successfully selected run condition and submitted', "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog(' Selecting run condition and submitting', "Failed to execute", Status.Pass)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    #===========================================================================
    # STEP-4: SELLER REQUIRED FIELDS
    #===========================================================================
    def Copart_ReceivingApp_067(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.step1_vin_screen(self)
            Defect.step2_basic_info(self)
            Defect.step3_run_condition(self)
            if(Element('STEP4_SUBTITLE', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('verify the title on step 4', "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog(' Verifying the title on step4', "Failed to execute", Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_068(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.step1_vin_screen(self)
            Defect.step2_basic_info(self)
            Defect.step3_run_condition(self)
            Defect.step4_seller_required_fields(self,0,True)
            if(Element('STEP5_SUBTITLE', self.mobileBrowser).isElementPresent()):
                self.report.addReportLog('Submitted without any configurations on Step 4 and land on Step 5', "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('Failed to submit without any configurations on Step 4 and land on Step 5', "Successfully executed", Status.Pass)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_069(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.step1_vin_screen(self) #PASS VIN NO
            Defect.step2_basic_info(self) #PASS HAS ENGINE NO
            Defect.step3_run_condition(self)
            Element('id:SRF_6.', self.mobileBrowser).bringToVisibility()
            x= Element('id:SRF_6.', self.mobileBrowser).isenabled()
            Element('id:SRF_17.', self.mobileBrowser).bringToVisibility()
            y= Element('id:SRF_17.', self.mobileBrowser).isenabled()
            Element('id:SRF_last.', self.mobileBrowser).bringToVisibility()
            z= Element('id:SRF_last.', self.mobileBrowser).isenabled()
            if(x==False and y==False and z==False):
                self.report.addReportLog('After Step 1, Step2 the Engine & VIN values cannot be changed on Step 4 and if 1st section has a missing piece, No parts missing is No and cannot be changed', "Successfully executed", Status.Pass)
                pass
            else:
                self.report.addReportLog('After Step 1, Step2 the Engine & VIN values able to be change on Step 4', "Failed to execute", Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_070(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.step1_vin_screen(self)
            Defect.step2_basic_info(self)
            Defect.step3_run_condition(self)
            Element('id:SRF_last.', self.mobileBrowser).bringToVisibility()
            Element('id:SRF_last.', self.mobileBrowser).click()
            Element('id:SRF_0.', self.mobileBrowser).bringToVisibility(True)
            Element('id:SRF_0.', self.mobileBrowser).click()
            Element('id:SRF_1.', self.mobileBrowser).bringToVisibility()
            Element('id:SRF_1.', self.mobileBrowser).click()
            self.report.addReportLog('Selected Cannot open hood and cannot open pass', 'Successfully Executed', Status.Pass)
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    def Copart_ReceivingApp_071(self):
        try:
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '3')
            Defect.step3_run_condition(self)
            Defect.navigateToStep_helper(self, '3')
            Defect.step3_run_condition(self,'1') #1 is for other value for run condition even though it's the same lot.
            if(Element('STEP4_SUBTITLE', self.mobileBrowser).isElementPresent()):
                #REPORT SUCCESS
                self.report.addReportLog('Selecting a run condition value and changing the value by going back', 'Successfully Executed', Status.Pass)
                pass
            else:
                #REPORT FAILURE
                self.report.addReportLog('Selecting a run condition value and changing the value by going back', 'Failed to Execute', Status.Fail)
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
        
    def Copart_ReceivingApp_072(self):
        try:
            missing=''
            Defect.login(self)
            Defect.get_receivinglist(self)
            Defect.select_lot(self)
            Defect.navigateToStep_helper(self, '3')
            Defect.step3_run_condition(self)
            if(Element('STEP4_SUBTITLE', self.mobileBrowser).isElementPresent()):
                dictionary = ['LeftMenu_HomeScreen','LeftMenu_Settings','Sign_Out']
                Defect.click_leftdrawer(self)
                missing = Defect.dictionary_checkifPresent_helper(self, dictionary, 'Left Menu', True)
                if(missing==''):
                    #REPORT SUCCESS
                    self.report.addReportLog('checking left navigation drawer elements', 'Successfully Executed', Status.Pass)
                    pass
                else:
                    #REPORT FAILURE
                    self.report.addReportLog('checking left navigation drawer elements. Missing:'+missing, 'Failed to Execute', Status.Fail)
                    pass
                pass
            else:
                #REPORT FAILURE
                self.report.addReportLog('checking left navigation drawer elements.Unable to land on STEP 4', 'Failed to Execute', Status.Fail) 
                pass
            pass
        except AssertionError as ase:
            self.report.addReportLog('', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)
            pass
    
    
        
            
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()