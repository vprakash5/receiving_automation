'''
Created on Jun 24, 2015

@author: 175730
'''
import unittest
from org.cognizant.mobile.scripthelper.Utility import Utility
from org.cognizant.mobile.execution.TestReporting import ResultStore, Status,\
    TestReport
from org.cognizant.mobile.testcase.BaseState import BaseState
from org.cognizant.mobile.scripthelper.ObjectRepository import Element
import random


class DefectMSetII(unittest.TestCase):


    def setUp(self):
        #print('In setup')
        self.lib = Utility()
        
        print(self.id())
        self.report = ResultStore(self.id(), self.mobileBrowser)
        
        # Invoking BaseState
        baseState = BaseState()
        if (self.testData['BaseState'] == 'PublicUS'):
            baseState.PublicUS(self.mobileBrowser)
            pass
        elif (self.testData['BaseState'] == 'PublicUK'):
            baseState.PublicUK(self.mobileBrowser)
            pass        
        pass        

    def tearDown(self):
        self.mobileBrowser.quit()
        TestReport.addToReportList(self.report)    
        pass
    
    # Place Holder
    
    def auto_mma_780_uk_member(self):
        try:

            Element('Saved_Search_Home_Android', self.mobileBrowser).click()
            self.report.addReportLog('Tap Saved Search button', 'Tapped Saved Search', Status.Pass)
            Element('Btn_Settings_Android', self.mobileBrowser).click()
            Element('Sign_In_UserName_Android', self.mobileBrowser).waitForElement(2)
            Element('Sign_In_UserName_Android', self.mobileBrowser).setText(self.lib.fetchDataValue(self.testData, 'UserName'))            
            Element('Sign_In_Password_Android', self.mobileBrowser).setText(self.lib.fetchDataValue(self.testData, 'Password'))
            Element('Sign_In_Button_Android', self.mobileBrowser).click()
            self.report.addReportLog('Tap Signin button with expired password', 'Tapped Signin button', Status.Pass) 
            #self.lib.signIn(self.mobileBrowser, self.testData)
            #Element('Message_Android', self.mobileBrowser).waitForElement(2)
            Element('name:Your password has expired', self.mobileBrowser).waitForElement(2)
            sMessage=Element('Message_Android', self.mobileBrowser).getText()
            print("Password Expiry Message"+sMessage)
            
            self.assertTrue(Element('Message_Android', self.mobileBrowser).getText(), 'Your password has expired')            
            self.report.addReportLog('Password expiry message', 'Message:Your password has expired', Status.Pass)                        
            
            pass
        except AssertionError as ase:
            self.report.addReportLog('TC failed', str(ase), Status.Fail)
            pass
        except Exception as exp:
            self.report.addReportLog('TC failed', 'Test Case failed due to unexpected exception: ' + str(exp), Status.Fail)  
            pass
        pass
    
    def auto_mma_780_us_member(self):
        self.auto_mma_780_uk_member()
    
    def checkingFilterOptionPopulated(self, filterObjectId, filterOpt):
        Element(filterObjectId, self.mobileBrowser).click()
            
        lstMake = Element('Filter_Value_Android', self.mobileBrowser).listElementByName()
            
        if(lstMake.__len__()!=0):
            if(lstMake.__len__()==1):
                if(lstMake[0]=='Any'):
                    self.report.addReportLog('Checking List of ' + filterOpt + ' in Filter option', filterOpt + ' option not listed', Status.Fail)
                    pass
                else:
                    self.report.addReportLog('Checking List of ' + filterOpt + ' in Filter option', filterOpt + ' listed: ' + str(lstMake), Status.Pass)
                    pass
                pass
            else:
                self.report.addReportLog('Checking List of ' + filterOpt + ' in Filter option', filterOpt + ' listed: ' + str(lstMake), Status.Pass)
                pass
            pass
        else:
            self.report.addReportLog('Checking List of ' + filterOpt + ' in Filter option', filterOpt + ' options not listed', Status.Fail)
            pass
            
        Element('Cancel_Button_Android', self.mobileBrowser).click() 
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()