'''
Created on April 28, 2015

@author: Cognizant Technology Solution
'''

import configparser, os

class TestConfiguration(object):
    '''
    Utility Class to read configuration Settings from TestConfiguration.cfg file
    '''
        
    def __init__(self):
        '''
        Constructor
        '''
        self.sFilePath = os.getcwd() + os.sep + 'TestConfiguration.cfg'
        pass
    
    config = None
    
    waitTime = 0
    
    @staticmethod
    def getConfigValue(sSection, sOption):
        '''
        Retrieves the execution information
        '''
        return TestConfiguration.config.get(sSection, sOption)
        pass   
    
    def readConfiguration(self):
        '''
        Reads the configuration file and stores execution data
        '''
        try:            
            TestConfiguration.config = configparser.ConfigParser()
            TestConfiguration.config.readfp(open(self.sFilePath)) 
            TestConfiguration.setGlobalWaitTime()  
        except IOError:            
            raise IOError('Unable to read Configuration file')   
        
    @staticmethod
    def setGlobalWaitTime():
            TestConfiguration.waitTime = int(TestConfiguration.getConfigValue('Run Settings', 'GlobalWaitTime'))
            pass 