'''
Created on Apr 13, 2015
@author: 209537
'''

from org.cognizant.mobile.execution.TestManager import Driver, Device
from org.cognizant.mobile.launcher.TestSuiteConfiguration import TestConfiguration
from org.cognizant.mobile.execution.ParallelRunner import Runner
from org.cognizant.mobile.execution.TestReporting import ReportSettings, TestReport, Status, SummaryStore, TestStatus, SummaryReport
from org.cognizant.mobile.scripthelper.ObjectRepository import ObjectRepository
from org.cognizant.mobile.execution.DataField import DataField
from datetime import datetime


if __name__ == '__main__': 
    
    
    # Initializing Test Run
      
    try:         
        # Performs Framework setup for Reporting changes
        reportSetting = ReportSettings()
        reportSetting.createReportFolder()        
        reportSetting.readCSSContent()  
        
        # Reading Test Configuration Details from TestConfiguration.cfg file
        testConfiguration = TestConfiguration()
        testConfiguration.readConfiguration()
        print(testConfiguration.getConfigValue('Run Settings', 'Driver'))
      
        # Fetching Execution Information
        driver = Driver()
        driver.readXMLSchema('Driver')
        driverDataList = driver.getExecutionList()
         
        # Read Device List
        device = Device()        
        device.readDeviceInformation('Device')
         
        # Iterating through driverData and assigning to Thread
        deviceList = []
        runnerList = []    
          
        # Load Object Repository - for selected application         
        repository = ObjectRepository()
        repository.initializeObjectRepository(TestConfiguration.getConfigValue('Run Settings', 'Application'))
        DataField()
         
        # Checking if Threading is set
         
        sThreading = TestConfiguration.getConfigValue('Run Settings', 'Threading')        
        if(sThreading.strip()=='Yes'):            
            for driverData in driverDataList:                
                # Checking device and initializing Thread
                  
                if(deviceList.count(driverData['Device']) == 0):
                    deviceList.append(driverData['Device'])
                    runner = Runner(driverData['Device'])
                    runnerList.append(runner)
                pass
              
            # Adding Test Cases to Thread
            for driverData in driverDataList:
                for runner in runnerList:
                    if(runner.getDeviceName()== driverData['Device']):
                        runner.addDriver(driverData)
                        pass
                    pass
                pass 
               
        else:             
            # Initializing Thread
            runner = Runner(deviceName="Any")        
            pass
           
            # Adding Execution list
            for driverData in driverDataList:
                runner.addDriver(driverData)
                pass
            runnerList.append(runner)
            pass
        
        # Starting the Thread for Execution 
        startTime = datetime.now()  
        for runner in runnerList:
            runner.setDaemon(True)
            runner.start()
            pass
        
        for runner in runnerList:
            runner.join(timeout=None)   
            pass
        endTime = datetime.now()
        
        suiteExecutionTime = endTime - startTime
        #sExecTime = str(suiteExecutionTime.hour) + " hours " + str(suiteExecutionTime.minute) + " minutes "  + str(suiteExecutionTime.second) + " seconds"
        
        # Adding Execution Data to Summary Store
        summaryStore = SummaryStore()
        
        for result in TestReport.getReportList():
            
            testCaseId = result.getReportStoreID()
            
            for driver in driverDataList:
                if(testCaseId.find(driver['TC_ID'])!=-1):
                    description = driver['Description']
                    try:
                        indexFail = result.getStatusList().index(Status.Fail)
                        testStatus = TestStatus.Failed
                        pass
                    except:
                        testStatus = TestStatus.Passed
                        pass
                        
                    summaryStore.addTestCaseInfo(driver['TC_ID'], description, testStatus, result.getExecutionTime())
                    break
                    
                pass
            pass
            
        # Creating Summary Reporting
        summaryReport = SummaryReport()
        summaryReport.AddReportHeader()
        summaryReport.AddInfoHeader(SummaryStore.totalTests, SummaryStore.testCasePassed, SummaryStore.testCaseFailed, str(suiteExecutionTime))
        stepNo = 1
        for testReport in summaryStore.getSummaryStoreList():            
            summaryReport.addReportLog(stepNo, testReport['TC_ID'], testReport['Description'], testReport['TestStatus'], testReport['ExecutionTime'])
            stepNo = stepNo + 1
            pass 
        pass
    except Exception as exp:        
        print(exp)
 
     
     
     

   


