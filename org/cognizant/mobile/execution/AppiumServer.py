'''
Created on May 25, 2015

@author: 209537
'''
from threading import Thread
from org.cognizant.mobile.launcher.TestSuiteConfiguration import TestConfiguration
import os
from time import sleep

class AppiumServer(Thread):
    '''
    classdocs
    '''


    def __init__(self, deviceInfo, currentPort):
        '''
        Constructor
        '''
        Thread.__init__(self)
        self.deviceInfo = deviceInfo
        self.currentPort = currentPort
        pass
    
    def run(self):
        # Running on Appium Command
        projectDir = os.getcwd()
        nodeDir = TestConfiguration.getConfigValue('Run Settings', 'NodePath')
        #os.chdir(nodeDir)
        appiumServerCommand = 'cmd /c ' + nodeDir + os.sep + 'node.exe ' + nodeDir + os.sep + 'node_modules\\appium\\bin\\appium.js --address 127.0.0.1 --app ' + TestConfiguration.getConfigValue(TestConfiguration.getConfigValue('Run Settings', 'Application') + '-' + self.deviceInfo['OS'], 'app') + ' -U ' + self.deviceInfo['UDID'] + ' -p ' + str(self.currentPort + 1) + ' -bp ' + str(self.currentPort + 5)
        print(appiumServerCommand)
        proc = os.system(appiumServerCommand)  
        os.chdir(projectDir) 
        #sleep(10)   
        
        
        pass
    
        