'''
Created on May 21, 2015

@author: 209537
'''
from org.cognizant.mobile.launcher.TestSuiteConfiguration import TestConfiguration
import os

class DataField(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        sFileName = TestConfiguration.getConfigValue('Run Settings', 'DataField')
        self.sDataFile = os.getcwd() + os.sep + sFileName
        self.readXMLSchema()
        pass
    
    def readXMLSchema(self):
        '''
        To do :- Reading Execution driver file
        '''
        try:
            # Fetching document root
            import xml.etree.ElementTree as eTree            
            self.tree = eTree.parse(self.sDataFile)    
            rootDriver = self.tree.getroot()
            
            DataField.rows = [] #list of rows - to be initialized            
             
            if(rootDriver.find('FieldSet/field') != -1): 
                DataField.rows = rootDriver.findall('FieldSet/field')
                pass
        except IOError as ioe:
            raise ioe 
        except Exception as exp:
            raise exp
        pass
    
    @staticmethod    
    def getValueFormat(sName):
        '''
        
        '''
        for row in DataField.rows:
            if(row.find('name').text == sName):
                return row.find('type').text
                pass
            pass
        return 'Not there' 