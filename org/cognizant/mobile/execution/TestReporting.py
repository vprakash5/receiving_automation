'''
Created on May 7, 2015

@author: 209537
'''
from enum import Enum
import os
from datetime import datetime
from org.cognizant.mobile.launcher.TestSuiteConfiguration import TestConfiguration

class ResultStore(object):
    '''
    classdocs
    '''
    
    def __init__(self, testCaseID, mobileBrowser):
        '''
        Constructor
        '''
        self.testCaseID = testCaseID
        self.mobileBrowser = mobileBrowser
        self.stepDescList = []
        self.stepInfoList = []
        self.statusList = []
        self.imagePathList = []
        pass
    
    def getReportStoreID(self):
        return self.testCaseID
    def setExecutionTime(self, delta):
        self.sExecutionTime = str(delta) 
    def getExecutionTime(self):
        return self.sExecutionTime
    def addReportLog(self, stepDesc, stepInfo, status): 
        '''
        Storing log for Test Scenario
        '''
        self.stepDescList.append(stepDesc)
        self.stepInfoList.append(stepInfo)
        self.statusList.append(status) 
        if(status == Status.Pass):
            if(TestConfiguration.getConfigValue('Report Settings', 'IncludePassedStepScreenShot')=='True'):                   
                self.imagePathList.append(self.IncludeScreenShot(stepInfo))            
                pass
            else:
                self.imagePathList.append(None) 
                pass
            pass
        elif(status == Status.Fail):
            if(TestConfiguration.getConfigValue('Report Settings', 'IncludeFailedStepScreenShot')=='True'):
                self.imagePathList.append(self.IncludeScreenShot(stepInfo))            
                pass
            else:
                self.imagePathList.append(None) 
                pass
            pass
        elif(status == Status.Done):
            if(TestConfiguration.getConfigValue('Report Settings', 'IncludeDoneStepScreenShot')=='True'):
                self.imagePathList.append(self.IncludeScreenShot(stepInfo))            
                pass
            else:
                self.imagePathList.append(None) 
                pass
            pass
        else:
            if(TestConfiguration.getConfigValue('Report Settings', 'IncludeWarningStepScreenShot')=='True'):
                self.imagePathList.append(self.IncludeScreenShot(stepInfo))            
                pass
            else:
                self.imagePathList.append(None) 
                pass
            pass    
        pass
    
    def IncludeScreenShot(self, stepInfo):
        # Taking screenshot based on option
        path = os.sep + 'Images' + os.sep + self.testCaseID +stepInfo + '.png'
        transtab = str.maketrans(' :\'', '___') 
        path = path.translate(transtab)
        self.mobileBrowser.save_screenshot(ReportSettings.getReportPath() + path)
        return path        
        pass
    
    def getTestStepDescriptionList(self):
        return self.stepDescList
    pass

    def getStepInformationList(self):
        return self.stepInfoList
    pass

    def getStatusList(self):
        return self.statusList
    pass

    def getImagePathList(self):
        return self.imagePathList
    pass

class SummaryStore(object):
    
    def __init__(self):
        self.testReportList = []
        pass
    
    totalTests = 0
    testCasePassed = 0
    testCaseFailed = 0
    
    def  addTestCaseInfo(self, testCaseId, testCaseDesc, testStatus, testExecutionTime):
        '''
        Adding Summary Report Record
        '''
        testReport = {}
        testReport['TC_ID'] = testCaseId
        testReport['Description'] = testCaseDesc
        testReport['TestStatus'] = testStatus
        SummaryStore.totalTests = SummaryStore.totalTests + 1
        if(testStatus == TestStatus.Passed):
            SummaryStore.testCasePassed = SummaryStore.testCasePassed + 1
            pass
        else:
            SummaryStore.testCaseFailed = SummaryStore.testCaseFailed + 1
            pass
        
        testReport['ExecutionTime'] = testExecutionTime
        self.testReportList.append(testReport)
        pass
    
    def getSummaryStoreList(self):
        return self.testReportList        
    pass

class TestReport(object):   
    
    reportList = []
    
    def __init__(self):     
        
        pass
    
    @staticmethod
    def addToReportList(report):        
        TestReport.reportList.append(report)
    pass

    
    
    @staticmethod
    def getReportList():
        return TestReport.reportList 
    pass   

class ReportSettings(object):
    
    reportPath = ''
    cssContent = ''
    def __init__(self):
        
        pass
    
    def createReportFolder(self):
        '''
        Creating Report Folder for each run
        '''
        
        resultPath = os.getcwd() + os.sep + '..' + os.sep + '..' + os.sep + '..' + os.sep + '..' + os.sep
        rootDir = resultPath
        os.chdir(resultPath)
        
        if not (os.path.isdir(resultPath + 'Result')):
            os.mkdir('Result')
            print('Result folder created')
            pass
        else:
            print('Result folder already exists')
            pass                      
        pass
        
        resultPath = resultPath + 'Result'
        os.chdir(resultPath)
        
        '''
        Creating Run folder
        '''
        timeStamp = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
        ReportSettings.reportPath = resultPath + os.sep + 'Run_' + timeStamp
        os.mkdir(ReportSettings.reportPath)
        os.mkdir(ReportSettings.reportPath + os.sep + 'Images')
        ''' Moving to project Root '''
        os.chdir(rootDir)    
        pass
    
    def readCSSContent(self):
        try:
            cssPath = ReportSettings.reportPath + os.sep+ '..' + os.sep + 'TestResultStyle.css'
            with open(cssPath) as f:
                for line in f.readlines():
                    ReportSettings.cssContent = ReportSettings.cssContent + line
            pass
        except:
            pass
    
    @staticmethod
    def getReportPath():
        return ReportSettings.reportPath        
        pass
    
class DetailedReport(TestReport):
    
    def __init__(self, name):
            self.name = name
            filePath = ReportSettings.getReportPath() + os.sep + 'Detailed_Report_' + name + '.html'
            self.fileReport = open(filePath, mode='w')    
            pass
    
    def AddReportHeader(self):
        '''
        
        '''
        self.fileReport.write('<html><meta charset=\'UTF-8\'><head><style>' + ReportSettings.cssContent + '</style><title>Automation Test Report - ' + self.name + '</title></head>')
        self.fileReport.write('<body class=\'body\'><table width=100%><tr width=\'100%\'><td width=\'100%\'class=\'header\'>' + self.name + '</td></tr></table>')
        pass
    
    def AddInfoHeader(self, device, os, app):
        '''
        
        '''
        self.fileReport.write('<br/><table width=100%><tr class=\'heading\'><th width=33%>Device</th><th width=34%>OS</th><th width=33%>Application</th></tr>')
        self.fileReport.write('<tr class=\'subheading\'><td>' + device + '</td><td>' + os + '</td><td>' + app + '</td></tr></table><br/>')
        self.fileReport.write('<table width=100%><tr class=\'heading\'><th width=10%>Step No.</th><th width=35%>Step Description</th><th width=40%>Actual Result</th><th width=10%>Step Status</th></tr>')             
        pass
    
    def AddStepLog(self, stepNo, stepDesc, stepInfo, status, screenShot=None):
        if status == Status.Pass:
            if screenShot==None:                           
                self.fileReport.write('<tr class=\'content\'><td width=10% class=\'justified\'>' + stepNo + '</td><td width=35% class=\'justified\'>' + stepDesc + '</td><td width=40% class=\'justified\'>' + stepInfo + '</td><td width=10% class=\'pass\'>Pass</td></tr>')
                pass
            else:
                self.fileReport.write('<tr class=\'content\'><td width=10% class=\'justified\'>' + stepNo + '</td><td width=35% class=\'justified\'>' + stepDesc + '</td><td width=40% class=\'justified\'>' + stepInfo + '</td><td width=10% class=\'pass\'><a href=\'' + ReportSettings.getReportPath() + screenShot +'\'>Pass</a></td></tr>')
                pass
            pass
        elif status == Status.Fail:
            if screenShot==None:
                self.fileReport.write('<tr class=\'content\'><td width=10% class=\'justified\'>' + stepNo + '</td><td width=35% class=\'justified\'>' + stepDesc + '</td><td width=40% class=\'justified\'>' + stepInfo + '</td><td width=10% class=\'fail\'>Fail</td></tr>')
                pass
            else:
                self.fileReport.write('<tr class=\'content\'><td width=10% class=\'justified\'>' + stepNo + '</td><td width=35% class=\'justified\'>' + stepDesc + '</td><td width=40% class=\'justified\'>' + stepInfo + '</td><td width=10% class=\'fail\'><a href="' + ReportSettings.getReportPath() + screenShot + '">Fail</a></td></tr>')
                pass
        elif status == Status.Done:
            if screenShot==None:
                self.fileReport.write('<tr class=\'content\'><td width=10% class=\'justified\'>' + stepNo + '</td><td width=35% class=\'justified\'>' + stepDesc + '</td><td width=40% class=\'justified\'>' + stepInfo + '</td><td width=10% class=\'done\'>Done</td></tr>')
                pass
            else:
                self.fileReport.write('<tr class=\'content\'><td width=10% class=\'justified\'>' + stepNo + '</td><td width=35% class=\'justified\'>' + stepDesc + '</td><td width=40% class=\'justified\'>' + stepInfo + '</td><td width=10% class=\'done\'><a href=\'' + ReportSettings.getReportPath() + screenShot + '\'>Done</a></td></tr>')
                pass
        else:
            if screenShot==None:
                self.fileReport.write('<tr class=\'content\'><td width=10% class=\'justified\'>' + stepNo + '</td><td width=35% class=\'justified\'>' + stepDesc + '</td><td width=40% class=\'justified\'>' + stepInfo + '</td><td width=10% class=\'warning\'>Warning</td></tr>')
                pass
            else:
                self.fileReport.write('<tr class=\'content\'><td width=10% class=\'justified\'>' + stepNo + '</td><td width=35% class=\'justified\'>' + stepDesc + '</td><td width=40% class=\'justified\'>' + stepInfo + '</td><td width=10% class=\'warning\'><a href=\'' + ReportSettings.getReportPath() + screenShot + '\'>Warning</a></td></tr>')
                pass
            pass           
        pass
    
    def EndReport(self):
        self.fileReport.write('</table></body></html>')

class SummaryReport(TestReport):
    '''
    Summary Reporting
    '''
    def __init__(self):
        filePath = ReportSettings.getReportPath() + os.sep + 'Summary Report.html'
        self.fileReport = open(filePath, mode='w')    
        pass
    
    
    def AddReportHeader(self):
        '''
        Adding Summary Report Header
        '''
        self.fileReport.write('<html><meta charset=\'UTF-8\'><head><style>' + ReportSettings.cssContent + '</style><title>Summary Report</title></head>')
        self.fileReport.write('<body class=\'body\'><div class=\'tab\'><table width=100%><tr width=\'100%\'><td width=\'100%\'class=\'header\'>Summary Report</td></tr></table>')
        pass
    
    def AddInfoHeader(self, totalTests, testPassed, testFailed, testExecutionTime):
        '''
        Add Sub Header Table
        '''
        self.fileReport.write('<br/><table width=100%><tr class=\'heading\'><th width=25%>Total Tests</th><th width=25%>Tests Passed</th><th width=25%>Test Failed</th><th width=25%>Test Execution Time</th></tr>')
        self.fileReport.write('<tr class=\'subheading\'><td>' + str(totalTests) + '</td><td>' + str(testPassed) + '</td><td>' + str(testFailed) + '</td><td>' + testExecutionTime + '</td></tr></table><br/>')
        self.fileReport.write('<table width=100%><tr class=\'heading\'><th width=10%>Test No.</th><th width=25%>Test Case ID</th><th width=35%>Test Case Description</th><th width=10%>Test Status</th><th width=20%>Execution Time</th></tr>')            
        pass 
    
    def addReportLog(self, stepNo, testCaseID, testCaseDesc, testCaseStatus, testExecutionTime):
        '''
        
        '''
        if testCaseStatus == TestStatus.Passed:                       
            self.fileReport.write('<tr class=\'content\'><td width=10% class=\'justified\'>' + str(stepNo) + '</td><td width=25% class=\'justified\'><a href=\'Detailed_Report_' + testCaseID + '.html\'>' + testCaseID + '</a></td><td width=35% class=\'justified\'>' + testCaseDesc + '</td><td width=10% class=\'pass\'>Passed</td><td width=20%>' + testExecutionTime + '</td></tr>')
            pass
        else:
            self.fileReport.write('<tr class=\'content\'><td width=10% class=\'justified\'>' + str(stepNo) + '</td><td width=25% class=\'justified\'><a href=\'Detailed_Report_' + testCaseID + '.html\'>' + testCaseID + '</a></td><td width=35% class=\'justified\'>' + testCaseDesc + '</td><td width=10% class=\'fail\'>Failed</td><td width=20%>' + testExecutionTime + '</td></tr>')
            pass
        pass  
    
    def EndReport(self):
        self.fileReport.write('</table></div></body></html>') 
    pass

class TestStatus(Enum):
    Passed = 'Passed'
    Failed = 'Failed'

class Status(Enum):
    Pass = 'Pass'
    Fail = 'Fail'
    Warning = 'Warning'
    Done = 'Done'
    pass