'''
Created on Apr 29, 2015

@author: 209537
'''

from threading import Thread
from org.cognizant.mobile.execution.ExecutionHelper import ExecutionHelper
from org.cognizant.mobile.execution.TestReporting import DetailedReport, TestReport
from org.cognizant.mobile.execution.TestManager import Device, TestData
from org.cognizant.mobile.scripthelper.BrowserSettings import BrowserSettings
from time import sleep
from org.cognizant.mobile.launcher.TestSuiteConfiguration import TestConfiguration
import os
import threading
 

class Runner(Thread):
    '''
    Thread object to run test cases
    '''


    def __init__(self, deviceName):
        '''
        Constructor
        '''
        Thread.__init__(self, group=None, target=None, name=deviceName, args=None, kwargs=None)
        ''' deviceName - Thread Name '''
        self.deviceName = deviceName 
        self.driverList = []
        '''self.testConfiguration = TestConfiguration()
        self.testConfiguration.readConfiguration()'''
        
        pass
    
    def addDriver(self, driver):
        '''
        Add Driver object 
        '''
        self.driverList.append(driver)
        pass
    
    
    def run(self):
        '''
        Thread Activity
        '''
        for driver in self.driverList:
            
            ''' 
            Fetching random module 
            '''
            cls = ExecutionHelper.getClass('org.cognizant.mobile.testcase.' + driver['Module'] + '.' + driver['Module'])
            
            # Fetching Test Data            
            
            ''' Initializing MobileDriver '''
            if (self.deviceName == 'Any'):
                threadDevice = Device.getDevice(driver['Device'])
            else:
                threadDevice = Device.getDevice(self.deviceName)
                
            ''' Creating Test Suite and adding Test Cases to Test Suite '''
            
            instance = cls()
            instance._testMethodName = driver['TC_ID']
            
            lock = threading.Lock()
            lock.acquire()
            parameters = BrowserSettings()
            projectDir = os.getcwd()
            parameters.StartAppiumServer(threadDevice)
            sleep(30)
            os.chdir(projectDir)
            lock.release()
            testData = TestData(driver['Module'], projectDir)
            
            instance.testData = testData.loadTestData(driver['TC_ID'])
            lock.acquire()
            mobileBrowser = parameters.GetMobileBrowser(threadDevice)
            lock.release()
            instance.mobileBrowser = mobileBrowser
            instance.setUp()
            try:
                methodToCall = getattr(cls, driver['TC_ID'])
                methodToCall(instance)
            except AssertionError as ae:
                print(ae)
                
            instance.tearDown()
            
            
            ''' Running Test Suite '''
                      
            
            ''' Preparing Detailed Report '''
            detRep = DetailedReport(driver['TC_ID'])
            detRep.AddReportHeader()
            detRep.AddInfoHeader(threadDevice['UDID'], threadDevice['OS'], TestConfiguration.getConfigValue('Run Settings', 'Application'))
            cnt = 1
            for result in TestReport.getReportList():
                print(result.getReportStoreID())
                print(driver['TC_ID'])
                if(result.getReportStoreID().find(driver['TC_ID'])!=-1):
                    for cnt in range(0, result.getStatusList().__len__()):
                        detRep.AddStepLog(str(cnt + 1), result.getTestStepDescriptionList()[cnt], result.getStepInformationList()[cnt], result.getStatusList()[cnt], result.getImagePathList()[cnt])
                        pass
                    pass
                pass
            
            detRep.EndReport()
        pass
        
        
    
    def getDeviceName(self):
        '''
        Return Device Name Representing Thread
        '''
        return self.deviceName
        pass        
