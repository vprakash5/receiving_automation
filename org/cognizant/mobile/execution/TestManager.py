'''
Created on Apr 29, 2015

@author: 209537
'''

import os
'''from org.cognizant.mobile.execution.DataStore import DriverData, DeviceData'''
from org.cognizant.mobile.launcher.TestSuiteConfiguration import TestConfiguration

''' TestManager Class definition'''

class TestManager(object):
    '''
    This class is super class for fetching data across TestManager.xml 
    '''
    def __init__(self):        
        pass
    
    
    def readXMLSchema(self, sRoot):
        '''
        To do :- Reading Execution driver file
        '''
        try:
            # Fetching document root
            import xml.etree.ElementTree as eTree            
            self.tree = eTree.parse(self.sDataFile)    
            rootDriver = self.tree.getroot()
            
            self.rows = [] #list of rows - to be initialized            
             
            if(rootDriver.find('Row_ID') != -1):                        
                for row in rootDriver:
                    self.rows.append(row)
            
            if(self.rows != None):
                self.initializeParameters()
                pass
            else:
                raise IOError(sRoot + ' schema not present')                                                
           
            pass
        except IOError as ioe:
            raise ioe 
        except Exception as exp:
            raise exp
        pass
    
    def initializeParameters(self):
        '''
        Initialize the table Header
        '''
        try:
            self.lstRowTags = []        
            
            # Initialize Tag Headers    
            for params in self.rows[0]:
                self.lstRowTags.append(params.tag)    
                # print(params.tag)    
                pass
            pass
        except IOError as ioe:
            raise ioe
        except Exception as exp:
            raise exp
        pass
    

''' Driver Class Definition '''    
class Driver(TestManager): 
    
    def __init__(self):
        '''
        Constructor :- Initializing the data File Name
        '''
        TestManager.__init__(self)
        sFileName = TestConfiguration.getConfigValue('Run Settings', 'Driver')
        self.sDataFile = os.getcwd() + os.sep + sFileName 
        pass        
    
    def getExecutionList(self):
        '''
        To do :- Reads the Driver and lists the test cases to be executed
        '''
        self.executionDataList = []
        
        for row in self.rows:
            data = {}
            for header in self.lstRowTags:
                data[header] = row.find(header).text
                #print(header + ": " + data[header])
                pass
            if data['Execute']=='Yes':
                self.executionDataList.append(data)
        return self.executionDataList        
        pass 

''' Device Class Definition '''
class Device(TestManager):
    
    def __init__(self):
        '''
        Constructor
        '''
        TestManager.__init__(self)
        sFileName = TestConfiguration.getConfigValue('Run Settings', 'Device')
        self.sDataFile = os.getcwd() + os.sep + sFileName
        pass
    
    deviceList = []
    
    def readDeviceInformation(self, sRoot='Device'):
        # Reading and initializing Header
        TestManager.readXMLSchema(self, sRoot)
        
        # Reading the device Information and storing to a dictionary
        
        #self.driverDataList = []
        for row in self.rows:
            device = {}
            for header in self.lstRowTags:
                '''print(row.find(header))'''
                if(row.find(header).text!=None):
                    device[header] = row.find(header).text
                    # print(header + ": " + device[header])
                    pass
                else:
                    device[header] = ''
                    #print(header + ": " + device[header]) 
                    pass
                    
            Device.deviceList.append(device)
            pass      
        pass
     
    @staticmethod    
    def getDevice(deviceId):
        for device in Device.deviceList:
            if(device['DeviceID'] == deviceId):
                return device
            pass
        return None 
        
        
        
    pass

class TestData(TestManager):
    
    def __init__(self, sModule, projectDir):
        '''
        Constructor
        '''
        self.sModule = sModule
        TestManager.__init__(self)
        sFileName = sModule + '.xml'
        self.sDataFile = projectDir + os.sep + sFileName
        pass
    
    def loadTestData(self, sTestCase):
        '''
        Read Test Case Data and Populate to a Dictionary
        '''
        TestManager.readXMLSchema(self, self.sModule)
        testData = {}
        for row in self.rows:
            
            if(row.find('TC_ID').text == sTestCase):
                for header in row:
                    testData[header.tag] = row.find(header.tag).text
                    pass                    
                pass
            pass
        return testData
        
    
    def populateTestData(self, sTestCase):
        '''
        Populate Test Data
        '''
        pass
    
        
        
    
    