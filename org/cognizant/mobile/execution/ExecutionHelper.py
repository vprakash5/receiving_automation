'''
Created on Apr 29, 2015

@author: 209537
'''


class ExecutionHelper(object):
    '''
    classdocs
    '''
    
    def __init__(self):
        '''
        Constructor
        '''        
        pass
    
    @staticmethod
    def getClass(className):
        '''
        Returns Class Object
        '''
        parts = className.split('.')
        module = ".".join(parts[:-1])
        m = __import__( module )
        for comp in parts[1:]:
            m = getattr(m, comp)            
        return m
        
        pass
    
    
