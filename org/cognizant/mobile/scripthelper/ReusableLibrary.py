'''
Created on Apr 15, 2015

@author: 209537
'''

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from org.cognizant.mobile.launcher.TestSuiteConfiguration import TestConfiguration

#from org.cognizant.mobile.launcher.TestSuiteConfiguration import TestConfiguration

class ReusableLibrary(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''
    pass
    
    @staticmethod
    def isUIObjectReady(by, criteria, mobileDriver):
        
        bReturn=False;        
        try:  
            wait = WebDriverWait(mobileDriver, TestConfiguration.waitTime)         
            element = wait.until(EC.presence_of_element_located((by, criteria)))
            bReturn=True   
        except NoSuchElementException as noe:
            print("Exception::"+ noe)
            bReturn= False        
        except Exception as e:            
            print("Exception::"+ str(e))
            bReturn= False        
        return bReturn