'''
Created on May 21, 2015

@author: 209537
'''
from org.cognizant.mobile.scripthelper.ObjectRepository import Element
from org.cognizant.mobile.execution.DataField import DataField
from datetime import datetime, date, timedelta
#from _random import Random
import random
from org.cognizant.mobile.execution.TestReporting import Status
from _ast import Assert

class Utility(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        pass
    
    def getQuickPicksCount(self, category, mobileDriver):
        try:
            print('Get Quick Picks Category Lot Count')
            # Category of Quick Picks
            elementCategory = Element('xpath://android.widget.LinearLayout[android.widget.TextView[@text=\'' + category + '\']]', mobileDriver).getElement()
            quickPicksCount = Element('Quick_Picks_Count', elementCategory).getElement().text
            quickPicksCount = quickPicksCount.replace('(', '')
            quickPicksCount = quickPicksCount.replace(')', '')
            return int(quickPicksCount)
            
        except Exception as exp:
            print(str(exp))
        pass
    
    def signIn(self, mobileDriver, dataDict):
        '''
        A method for Member SignIn 
        '''
        # Clicking on Primary Sign In field
#         Element('Sign_In_Primary_Android', mobileDriver).click()        
        Element('Login_Username', mobileDriver).bringToVisibility()
        Element('Login_Username', mobileDriver).setText(self.fetchDataValue(dataDict, 'UserName'))
        Element('Login_Password', mobileDriver).bringToVisibility()
        Element('Login_Password', mobileDriver).setText(self.fetchDataValue(dataDict, 'Password'))
        Element('Login_SignIn_Btn', mobileDriver).bringToVisibility()
        Element('Login_SignIn_Btn', mobileDriver).click()        
        pass
    
    def FilterDataInSavedSearch(self, mobileBrowser, testData, report):
        try:
            Element('Filter_LotDetails_Android', mobileBrowser).click()
            
            if(Element('Filter_Option_LotDetails_Android', mobileBrowser).isElementPresent()):
                report.addReportLog('Checking Filters window appeared', 'Filters window appeared', Status.Pass)
                
                # Selecting Filter option 
                Element('name:' + self.fetchDataValue(testData, 'FilterCriteria'), mobileBrowser).bringToVisibility()
                Element('name:' + self.fetchDataValue(testData, 'FilterCriteria'), mobileBrowser).click()
                
                # Selecting value to be filtered
                Element('name:' + self.fetchDataValue(testData, 'FilterData'), mobileBrowser).bringToVisibility()
                Element('name:' + self.fetchDataValue(testData, 'FilterData'), mobileBrowser).click()
                
                # Navigating to Filter main menu
                if (Element('name:' + self.fetchDataValue(testData, 'FilterCriteria'), mobileBrowser).isElementPresent()):
                    Element('name:' + self.fetchDataValue(testData, 'FilterCriteria'), mobileBrowser).click()
                    pass
                
                
                # Apply filter 
                Element('ApplyFilter_Button_Android', mobileBrowser).waitForElement(3) 
                Element('ApplyFilter_Button_Android', mobileBrowser).click()
            
                report.addReportLog('Checking Count of lots', 'Lots appeared under criteria: ' + Element('PageText_Android', mobileBrowser).getText(), Status.Pass)
                                
                pass
            else:
                report.addReportLog('Checking Filters window appeared', 'Filters window did not appear', Status.Fail)
                pass
            pass
        except Exception as exp:
            raise exp
        pass
        
    def fetchDataValue(self, data, key, listValidValues=None):
        sValue = ''
        
        try:
            sValue = data[key]
            if(sValue==''):
                raise KeyError
            pass
        except KeyError:
            sFormat = DataField.getValueFormat(key)
            if(sFormat=='#SELECTRANDOMVAL'):
                sValue = self.generateValueByFormat(sFormat, listValidValues)  
                pass
            else:                 
                sValue = self.generateValueByFormat(sFormat)
                pass
                   
            pass
        return sValue
        pass
    
    def generateValueByFormat(self, sFormat, listValidValues=None):
        
        sVal = ''
        if(sFormat.find("#TEXT")!=-1):
            sVal = "TextFieldValue"
            pass
        elif (sFormat.find("#NUMBER")!=-1):
            iDigits = int(sFormat.replace('#NUMBER', ''))
            sVal = self.getFixedLengthDigit(iDigits)            
        elif (sFormat.find('#ALPHANUM')!=-1):
            sVal = "T" + self.getTimeStamp()
        elif(sFormat.find("#SELECTRANDOMVAL")!=-1):
            return listValidValues[random.randint(0, listValidValues.__len__())]
        elif (sFormat.find("#DATEPASTERLY1")!=-1):
            return self.getDate("#DATEPASTERLY1")
        elif (sFormat.find("#DATEPAST")!=-1):
            iDays = int(sFormat.replace('#DATEPAST', ''))
            return self.getDate('#DATEPAST', iDays)
        elif (sFormat.find("#DATEFUTURE")!=-1):
            iDays = int(sFormat.replace('#DATEFUTURE', ''))
            return self.getDate('#DATEFUTURE', iDays)
        elif (sFormat.find("#TODAY")!=-1):
            return self.getDate('#TODAY')
        elif (sFormat.find("#YEAR")!=-1):
            return "2010"
        elif (sFormat.find("#RANDNO")!=-1):
            return "9" + self.getTimeStamp()
        elif (sFormat.find('#HARD')!=-1):
            sVal = sFormat.replace('#HARD', '')
        elif (sFormat.find('#EMAIL')!=-1):
            sVal = self.getTimeStamp() + "@test.com"
            pass
        return sVal            
        pass
    
    def getFixedLengthDigit(self, iDigit):        
        iMin = 10**(iDigit-1)
        iMax = (10**iDigit)-1
        iVal = random.randint(iMin, iMax)
        #print(iVal)
        return str(iVal)
        pass
    
    def getTimeStamp(self):
        sTimeStamp = datetime.now().strftime('%Y%m%d%H%M%S')
        return sTimeStamp
        pass
    
    def getDate(self, sFormat, iDays = 0):
        if(sFormat=='#DATEPAST'):
            sDate = date.today() - timedelta(iDays)
            print(sDate.strftime('%m%d%Y'))
            return sDate
            pass
        elif(sFormat=='#DATEFUTURE'):
            sDate = date.today() + timedelta(iDays)
            print(sDate.strftime('%m%d%Y'))
            return sDate
            pass
        elif(sFormat=='#TODAY'):
            sDate = date.today()
            print(sDate.strftime('%m%d%Y'))
            return sDate
            pass
        elif(sFormat=='#DATEPASTERLY1'):
            iDays = random.randint(1, 365)
            sDate = date.today() - timedelta(iDays)
            print(sDate.strftime('%m%d%Y'))
            return sDate
            pass
      
    def fillRegistrationUKPageOne(self, mobileBrowser, testData, report):
        try:
                
            strEmail = self.fetchDataValue(testData, 'Email')
                
            # Entering First name
            Element('Email_Registration_Android', mobileBrowser).setText(strEmail)
            report.addReportLog('Entering Email id', 'Email ID entered', Status.Done)
                
            # Entering Last name
            Element('Confirm_Email_Registration_Android', mobileBrowser).setText(strEmail)
            report.addReportLog('Entering Confirm Email id', 'Confirm Email ID entered', Status.Done)
                
            # Entering First Name
            Element('First_Name_Registration_Android', mobileBrowser).setText(self.fetchDataValue(testData, 'FirstName'))
            
            # Entering Last Name    
            Element('Last_Name_Registration_Android', mobileBrowser).setText(self.fetchDataValue(testData, 'LastName'))
                
            # Entering Company Name
            Element('Company_Registration_Android', mobileBrowser).setText(self.fetchDataValue(testData, 'Company'))
            
            # Entering Phone number
            sPhoneNumber = self.fetchDataValue(testData, 'Phone')
            #print('Phone: ' + sPhoneNumber)
            Element('Phone_Register_Android', mobileBrowser).setText(sPhoneNumber)
            report.addReportLog('Entering Phone number', 'Entered Phone number: ' + sPhoneNumber, Status.Pass)
            
            # How Did you hear
            
            Element('How_Did_You_Hear_Register_Android', mobileBrowser).click()
            if(Element('HDYH_Option_Register_Android', mobileBrowser).isElementPresent()):
                report.addReportLog('Checking if Hear options appeared', 'How did you hear - options appeared', Status.Pass)
                Element('HDYH_Option_Register_Android', mobileBrowser).click()
                pass
            else:
                self.report.addReportLog('Checking if Hear options appeared', 'How did you hear - options did not appear', Status.Fail)
                pass
            
            
            # Navigating to 2nd page
            
            Element('Next_Button_Register_Android', mobileBrowser).bringToVisibility()
            Element('Next_Button_Register_Android', mobileBrowser).click()
            
            if(Element('Street_Registration_Android', mobileBrowser).isElementPresent()):                
                report.addReportLog('Checking if Registration UK 2nd page appeared ', 'Registration UK 2nd page appeared', Status.Pass)
                pass
            else:
                report.addReportLog('Checking if Registration UK 2nd page appeared ', 'Registration UK 2nd page did not appear', Status.Fail)
            pass
        
        except Exception as exp: 
            raise exp
        pass
          
        
        