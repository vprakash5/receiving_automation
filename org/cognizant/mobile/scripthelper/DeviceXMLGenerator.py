import xml.etree.ElementTree as etree
from org.cognizant.mobile.launcher.TestSuiteConfiguration import TestConfiguration
import os
import sys
import re

class GenerateDeviceXML():
    
    def __init__(self):
        self.adbPath = TestConfiguration.getConfigValue('Run Settings', 'ADBPath')
        self.deviceXMLFilePath = TestConfiguration.getConfigValue('Run Settings', 'Device')
        self.driverXMLFilePath = TestConfiguration.getConfigValue('Run Settings', 'Driver')
        
        if os.path.isfile(self.deviceXMLFilePath):
            os.remove(self.deviceXMLFilePath)
            print(self.deviceXMLFilePath,"file exists, removed!!!!!")
            
    # Generate Device.xml file with device details
    def CreateFile(self):
        if os.path.exists(self.deviceXMLFilePath) != True:
            deviceID = self.GetADBDeviceIDList()
            tmp = open(self.deviceXMLFilePath, mode='a')
            tmp.write('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n')
            tmp.write('<Device xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n')
            for i in range(len(deviceID)):
                tmp.write('\t<Row>\n'
                          '\t\t<DeviceID>Android_'+str(i)+'</DeviceID>\n'
                          '\t\t<DeviceType>Android Device</DeviceType>\n'
                          '\t\t<OS>Android</OS>\n'
                          '\t\t<UDID>'+deviceID[i]+'</UDID>\n'
                          '\t</Row>\n')
            
            tmp.write('</Device>')    
            tmp.close()
            
    # Get device ID's
    def GetADBDeviceIDList(self):
        idList = []
        query = self.adbPath+" devices"
        device = os.popen(query).readlines()
        
        if len(device) > 2:   
            device.pop(0)
            device.pop(len(device)-1)
            for element in device:
                idRegex = re.match('^\s*?([\w\.\:]+)\s+.*$', element)
                idList.append(idRegex.group(1))
            return idList    
        
        else:
            print("\n>>>>>>>>>> No android device connected, Terminating execution!!!<<<<<<<<<<\n")
            sys.exit()
    
    # Update Driver.xml in case we work on multiple connected devices   
    def UpdateDriverXML(self):
        if os.path.exists(self.driverXMLFilePath) == True:
            tree = etree.parse(self.driverXMLFilePath)
            root = tree.getroot()
            for child in root:
                for deviceAttr in child.iter('Device'):
                    deviceAttr.text = "Android_001"
                    
            tree.write(self.driverXMLFilePath, encoding="utf-8", xml_declaration=True)