'''

@author: Cognizant Technology Solutions
'''

from org.cognizant.mobile.launcher.TestSuiteConfiguration import TestConfiguration
from appium.webdriver.webdriver import WebDriver
from org.cognizant.mobile.execution.AppiumServer import AppiumServer
from multiprocessing import synchronize
import threading

class BrowserSettings(object):
    '''
    Class Implementation for initializing browsers and server for test case execution
    '''
    def __init__(self):
        '''
        Constructor
        '''    
           
        pass
    
    devicePortList = []
    portLastUsed = 0
    lock = threading.Lock() 
    
    def StartAppiumServer(self, deviceInfo=None):  
        # Initializing Current Port:
        
        BrowserSettings.lock.acquire()
        if(BrowserSettings.portLastUsed == 0):
            self.currentPort = 4723
            pass
                   
        # Checking if Device/port is already opened
        deviceInfoPresent = False
        for devPort in BrowserSettings.devicePortList:        
            # Checking Device Port 
            if(devPort['Device']==deviceInfo['DeviceID']):
                deviceInfoPresent = True
                self.currentPort = devPort['Port']
                break
                pass
            pass
        
        if not deviceInfoPresent:
            if not (BrowserSettings.portLastUsed == 0):
                self.currentPort = BrowserSettings.portLastUsed + 1
            AppiumServer(deviceInfo, self.currentPort).start()
            devicePort = {}
            devicePort['Device'] = deviceInfo['DeviceID']
            devicePort['Port'] = self.currentPort + 1
            BrowserSettings.devicePortList.append(devicePort)    
            BrowserSettings.portLastUsed = self.currentPort    
            pass
        pass
        BrowserSettings.lock.release()    
        '''deviceInfoPresent = False'''
        pass
    
    def GetMobileBrowser(self, deviceInfo):
        
        sApplication = TestConfiguration.getConfigValue('Run Settings', 'Application')  
        sPlatform = deviceInfo['OS'] 
        sApplicationSection = sApplication + '-' + sPlatform
        for portList in BrowserSettings.devicePortList:
            if(portList['Device'] == deviceInfo['DeviceID']):
                port = portList['Port']
                
        capabilities = {}
        capabilities['platformName'] =  sPlatform
        capabilities['deviceName'] =  sPlatform
        '''capabilities['androidDeviceSocket'] = '4723'''
        
        ''' Initializing app '''
        app = TestConfiguration.getConfigValue(sApplicationSection, 'app')
        if not (app==None):
            capabilities['app']=app
            pass        
        
        appPackage = TestConfiguration.getConfigValue(sApplicationSection, 'appPackage')
        if not (appPackage==None): 
            capabilities['appPackage'] = appPackage
            pass
        
        ''' Initializing app-Activity '''
        appActivity = TestConfiguration.getConfigValue(sApplicationSection, 'appActivity')  
        if not (appActivity==None):
            capabilities['appActivity'] = appActivity
            pass
         
        ''' Initializing app-WaitActivity'''
#         appWaitActivity = TestConfiguration.getConfigValue(sApplicationSection, 'appWaitActivity')
#         if not (appWaitActivity==None):  
#             capabilities['appWaitActivity'] =  appWaitActivity
#             pass
        
        udid = deviceInfo['UDID']
        if not (udid==''):
            capabilities['udid'] = udid
            pass
        print(deviceInfo['DeviceID'] + str(port)) 
        
        while(True):   
            try:    
                browser = WebDriver('http://127.0.0.1:' + str(port) + '/wd/hub', capabilities)
                return browser 
            except Exception as exp:
                if(str(exp).find('EPERM')==-1):
                    raise exp
                    break
                pass
                   
                
        pass
        
    def GetFileBrowser(self):
        
        pass    