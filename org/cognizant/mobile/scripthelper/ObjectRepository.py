'''
Created on May 11, 2015

@author: 209537
'''
import os
from org.cognizant.mobile.execution.TestManager import TestManager
from org.cognizant.mobile.launcher.TestSuiteConfiguration import TestConfiguration
from org.cognizant.mobile.scripthelper.ReusableLibrary import ReusableLibrary
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from locale import str
from time import sleep
from builtins import int
from selenium.webdriver.common.keys import Keys
'''from appium.webdriver.common.touch_action import TouchAction'''
from selenium.webdriver.common.touch_actions import TouchActions

class ObjectRepository(TestManager):
    '''
    Loads the object repository for the Application
    '''
    objectInfoList = []

    def __init__(self):
        '''
        Constructor
        '''
        TestManager.__init__(self)
        '''self.testConfiguration = testConfiguration'''
        sFileName = TestConfiguration.getConfigValue('Run Settings', 'ObjectRepository')
        self.sDataFile = os.getcwd() + os.sep + sFileName
        pass
    
    objectInfoList = []
    
    def initializeObjectRepository(self, sApplication):
        '''
        Loading Object repository
        '''
        # Reading Object Repository Schema
        TestManager.readXMLSchema(self, sApplication)
        for row in self.rows:
            objectInfo = {}
            for header in self.lstRowTags:
                objectInfo[header] = row.find(header).text
                pass
            ObjectRepository.objectInfoList.append(objectInfo)
            pass
        pass
    
class Element(object):
    
    def __init__(self, elementID, mobileBrowser):
        self.mobileBrowser = mobileBrowser
        if(elementID.find(':') == -1):
            for obj in ObjectRepository.objectInfoList:
                if(obj['Element_ID']==elementID):
                    self.baseElement = obj
                    break
                    pass
                pass
        else:
            self.baseElement = {}
            self.baseElement['ORStrategy'] = elementID[0:elementID.find(':')]
            self.baseElement['ORProperty'] = elementID[elementID.find(':') + 1:]
            pass
        print(self.baseElement['ORProperty'])
        pass
    
    # Returns the list of Element Names
    def listElementByName(self):
            byOption = Element.ORStrategyOptions[self.baseElement['ORStrategy']]
            self.lstElement = []
            self.tempElement = self.mobileBrowser.find_elements(byOption, self.baseElement['ORProperty'])
            bEndofList = False
            while not (bEndofList):
                self.tempElement = self.mobileBrowser.find_elements(byOption, self.baseElement['ORProperty'])
                
                if(self.lstElement.__len__()==0 or self.lstElement[self.lstElement.__len__()-1] != self.tempElement[self.tempElement.__len__()-1].text):
                    
                    for element in self.tempElement:
                        try:
                            index = self.lstElement.index(element.text)
                        except ValueError:
                            self.lstElement.append(element.text)            
                            pass                
                        pass
                    pass
                else:
                    bEndofList = True
                    pass
                
            
                touchAct = TouchActions(self.mobileBrowser)
                touchAct.flick(0, -120).perform()
                sleep(0.5)
                pass
            return self.lstElement
            pass
        
    def clear(self):
        self.getElement()
        
        pass
    
    def click(self):
        self.getElement()
        self.Element.click()
        
    def isenabled(self):
        self.getElement()
        return self.Element.is_enabled()
    
    def getSize(self):
        self.getElement()
        return self.Element.size()
    
    def isElementPresent(self):
        self.getElement()
        return self.elementPresent
    
    def presskey(self, keycode):
        print('PRESSING '+str(keycode))
        self.mobileBrowser.press_keycode(keycode)
        pass
    
    def setText(self, sValue):
        self.getElement()
        self.Element.send_keys(sValue)
        try:
            self.mobileBrowser.hide_keyboard()
            pass
        except:
            pass
        pass
    
    def setTextAndNumForVin(self, sValue):
        self.getElement()
        self.Element.clear()
        self.Element.send_keys("")
        self.Element.send_keys(sValue)
        self.mobileBrowser.hide_keyboard()
        self.mobileBrowser.press_keycode(66)
        try:
            self.mobileBrowser.hide_keyboard()
            pass
        except:
            pass
        
    def setTextForLP(self, sValue):
        self.getElement()
        self.Element.send_keys(sValue)
        try:
            self.mobileBrowser.hide_keyboard()
            pass
        except:
            pass
    
    def setNumeric(self, sNumeric):
        # Bring element to focus
        self.getElement()
        self.Element.send_keys("")
        
        # Parsing data to a set of key event
        for sNumber in list(sNumeric):
            number = int(sNumber)
            print(number)
            number = number + 144
            self.mobileBrowser.keyevent(number)
            pass
        
        try:
            self.mobileBrowser.hide_keyboard()
            pass
        except:
            pass
        pass
    
        
    
    def getText(self):
        self.getElement()
        return self.Element.text
        
    def bringToVisibility(self, bFlickUp=False, skipNoElementException=False):
        '''
        bFlickUp: Flicks up to top if true
        '''
        self.getElement()
        maxFlickCount = int(TestConfiguration.getConfigValue('Run Settings', 'MaxFlickCount'))
        flickCount = 0
        while not (self.elementPresent):
            if(bFlickUp):
                touchAct = TouchActions(self.mobileBrowser)
                touchAct.flick(0, 20).perform()
                
            else:
                touchAct = TouchActions(self.mobileBrowser) 
                touchAct.flick(0, -20).perform()
            flickCount = flickCount + 1
            if(flickCount >= maxFlickCount):
                print('MISSING ELEMENT')
#                 if(skipNoElementException):
#                     pass
#                 else:
#                     raise NoSuchElementException
#                 pass
                break            
            self.getElement()
                    
    def getElement(self):
        try:            
            byOption = Element.ORStrategyOptions[self.baseElement['ORStrategy']]            
            self.Element = self.mobileBrowser.find_element(byOption, self.baseElement['ORProperty']) 
            self.elementPresent = True 
            return self.Element         
        except NoSuchElementException:
            self.elementPresent = False                  
        pass
    
    def waitForElementToDisAppear(self):
        try:
            byOption = Element.ORStrategyOptions[self.baseElement['ORStrategy']]  
            while True:
                element = self.mobileBrowser.find_element(byOption, self.baseElement['ORProperty'])
                sleep(0.5)
        except NoSuchElementException as nse: 
            pass
            
    def waitForElement(self, intWaitPeriod=1):
        try:
            byOption = Element.ORStrategyOptions[self.baseElement['ORStrategy']]  
            for ctr in range(1, intWaitPeriod):
                if(ReusableLibrary.isUIObjectReady(byOption, self.baseElement['ORProperty'], self.mobileBrowser)):
                    break
        except NoSuchElementException as nse: 
            raise nse
    
    ORStrategyOptions = {'id': By.ID, 'name': By.NAME, 'className': By.CLASS_NAME, 'xpath': By.XPATH}
    