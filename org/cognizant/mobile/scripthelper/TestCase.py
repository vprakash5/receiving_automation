'''
Created on Apr 14, 2015

@author: 209537
'''
# 
# from selenium.common.exceptions import NoSuchElementException
# '''from org.cognizant.mobile.scripthelper.ReusableLibrary import ObjectRepository'''
# from org.cognizant.mobile.scripthelper.ReusableLibrary import ReusableLibrary
# from selenium.webdriver.common.by import By
# from selenium.webdriver.common.touch_actions import TouchActions

# 
# class TestCase():
#     
# 
#     def __init__(self):
#         '''
#         Constructor
#         '''        
#         pass    
#     
#     def login(self, mobileDriver):
#         try:
#             if(mobileDriver != None):
#                 '''reUsableLib = ReusableLibrary(mobileDriver)'''
#                 '''Waiting for Login screen to appear'''
#                 ReusableLibrary.isUIObjectReady(By.NAME, "Login", mobileDriver)
#                 
#                 '''Entering Username'''
#                 meUserName = mobileDriver.find_element_by_name(ObjectRepository.UserName())
#                 meUserName.send_keys("CXDAS")
#                 
#                 ''' Entering Password'''
#                 mePassword = mobileDriver.find_element_by_xpath(ObjectRepository.Password())
#                 mePassword.send_keys("copart1")
#                 
#                 '''Clicking on login Button'''
#                 meLoginButton = mobileDriver.find_elements_by_xpath(ObjectRepository.LoginButton())[1]
#                 meLoginButton.click()
#                 
#                 ReusableLibrary.isUIObjectReady(By.NAME, "Home", mobileDriver)                           
#                 
#                 '''Checking if Login is successful'''
#                 try: 
#                                      
#                     homeScreen = mobileDriver.find_element_by_name(ObjectRepository.HomeHeader())
#                     print("Logged in to the application successfully")
#                 except NoSuchElementException as noe:
#                     print("Login failed")
#                     pass
#                 pass
#         except NoSuchElementException as noe:
#             print ('Test failed due to unexpected exception: ', noe)  
#         pass
#     
#     def changeYard(self, mobileDriver):
#         
#         try:
#             if(mobileDriver != None):
#                 
#                 ''' Clicking Yard Option'''
#                 yardOption = mobileDriver.find_element_by_xpath(ObjectRepository.YardOption())
#                 yardOption.click()
#                 
#                 ''' Waiting for List of Yard to appear'''
#                 ReusableLibrary.isUIObjectReady(By.NAME, "Yard", mobileDriver)
#                 
#                 ''' Selecting Yard'''
#                 yardSelect = mobileDriver.find_element_by_name(ObjectRepository.SelectYard("12"))
#                 yardSelect.click()
#                 
#                 ''' Waiting for Home screen to appear'''
#                 ReusableLibrary.isUIObjectReady(By.NAME, "Home", mobileDriver)
#                 
#                 '''Validating Yard Change'''
#                 try:
#                     yardValidate = mobileDriver.find_element_by_name(ObjectRepository.YardValidation("12"))
#                     print("Yard changed successfully")
#                 except NoSuchElementException as noe:
#                     print("Yard is not changed successfully", noe)
#                 pass
#                 
#         except Exception as e:
#             print("Exception: ", e)
#         pass
#     
#     def selectLot(self, mobileDriver):
#         try:
#             if(mobileDriver != None):
#                 
#                 ''' Clicking on Receiving Image to fetch receiving list for the yard'''
#                 receivingImage = mobileDriver.find_element_by_xpath("//android.widget.ImageView[2]")
#                 receivingImage.click()
#                 
#                 ''' Waiting for List to load'''
#                 ReusableLibrary.isUIObjectReady(By.NAME, "Updating Tables...", mobileDriver)
#                 ReusableLibrary.isUIObjectReady(By.NAME, "Receiving", mobileDriver)
#                 
#                 ''' Scrolling the list'''
#                 touchAct = TouchActions(mobileDriver)
#                 touchAct.flick(0, -81).perform()
#                 touchAct.flick(0, -81).perform()
#                 touchAct.flick(0, -81).perform()
#                 
#                 pass
#         except NoSuchElementException as noe:
#             print("No Such Element Exception: ", noe)
#         except Exception as e:
#             print("Exception: ", e)
#         pass